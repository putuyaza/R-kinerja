-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 27 Agu 2019 pada 18.04
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `r-kinerja`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `IdJabatan` int(11) NOT NULL,
  `Jabatan` varchar(30) NOT NULL,
  `Tunjangan` double(8,0) DEFAULT NULL,
  `GajiPokok` double(8,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`IdJabatan`, `Jabatan`, `Tunjangan`, `GajiPokok`) VALUES
(1, 'admin', NULL, 3000000),
(2, 'marketing', NULL, 2300000),
(3, 'manager', 1000000, 60000000),
(4, 'CEO', 4000000, 7000000),
(5, 'public  relationship', NULL, 2300000),
(6, 'programer', NULL, 4000000),
(7, 'designer', NULL, 3000000),
(8, 'Tester dan Implementator', NULL, 3500000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jobdesk`
--

CREATE TABLE `jobdesk` (
  `KodeJobdesk` int(11) NOT NULL,
  `Jobdesk` text NOT NULL,
  `EstimasiKerja` int(11) NOT NULL,
  `Tanggal` date NOT NULL,
  `Catatan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jobdesk`
--

INSERT INTO `jobdesk` (`KodeJobdesk`, `Jobdesk`, `EstimasiKerja`, `Tanggal`, `Catatan`) VALUES
(1, 'bekerja', 1, '2019-08-01', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kinerja`
--

CREATE TABLE `kinerja` (
  `InvoiceGaji` int(11) NOT NULL,
  `NIK` bigint(20) NOT NULL,
  `KodeJobdesk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `NIK` bigint(20) NOT NULL,
  `IdJabatan` int(11) NOT NULL,
  `Nama` varchar(30) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Level` varchar(15) NOT NULL,
  `JenisKelamin` varchar(1) NOT NULL,
  `NoTelpon` varchar(13) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `TargetJam` int(11) NOT NULL,
  `TglLahir` date NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`NIK`, `IdJabatan`, `Nama`, `Username`, `Password`, `Level`, `JenisKelamin`, `NoTelpon`, `Email`, `TargetJam`, `TglLahir`, `Status`) VALUES
(5654709090876710, 2, 'super admin', 'superadmin', 'MTIzNDU2Nzg=', 'administrator', 'L', '089606450123', 'yputu19@yahoo.co.id', 160, '1998-06-11', 'Kontrak'),
(5654709090876711, 2, 'i wayan putu yasa', 'DEWIKADEK', 'MTIzNDU2Nzg=', 'karyawan', 'L', '6565', 'kadek12@tahoo.com', 160, '2019-08-12', 'kontrak'),
(5654709090876716, 8, 'kadeksutama', 'alit1', 'MTIzNDU2Nzg=', 'karyawan', 'L', '123', 'alit@gmail.com', 160, '2019-08-13', 'kontrak'),
(5654709090876789, 1, 'i wayan putu yasa', 'batulaju', 'MTEzMjQ1', 'administrator', 'L', '123', 'putu32yasa@gmail.com', 160, '2019-08-12', 'kontrak'),
(5654709090876790, 1, 'i kadek aldi', 'putuyasa1267', 'MTIzNDU2Nzg=', 'administrator', 'P', '087908989788', 'kadek12@tahoo.com787', 160, '2019-08-19', 'kontrak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Token` varchar(100) NOT NULL,
  `Date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_token`
--

INSERT INTO `user_token` (`id`, `Email`, `Token`, `Date_created`) VALUES
(1, 'putu32yasa@gmail.com', 'TGTpxC7NjDoKTi2wszSuLEeyelb1qV/BVfjiJRVTRBE=', 1566917582),
(2, 'putu32yasa@gmail.com', 'N30JNey0WKszEi53ulosrOowwWm9QhaQ5qeF4zQcIvA=', 1566919984),
(3, 'yputu19@yahoo.co.id', 'Z0X/xolxVVF2oPbTrGz35m4x78wW4qxZ1WUcKQ9QiVo=', 1566921058);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`IdJabatan`),
  ADD UNIQUE KEY `Jabatan` (`Jabatan`);

--
-- Indeks untuk tabel `jobdesk`
--
ALTER TABLE `jobdesk`
  ADD PRIMARY KEY (`KodeJobdesk`);

--
-- Indeks untuk tabel `kinerja`
--
ALTER TABLE `kinerja`
  ADD PRIMARY KEY (`InvoiceGaji`),
  ADD KEY `NIK` (`NIK`),
  ADD KEY `KodeJobdesk` (`KodeJobdesk`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`NIK`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `Username` (`Username`),
  ADD KEY `IdJabatan` (`IdJabatan`);

--
-- Indeks untuk tabel `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `IdJabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `jobdesk`
--
ALTER TABLE `jobdesk`
  MODIFY `KodeJobdesk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kinerja`
--
ALTER TABLE `kinerja`
  MODIFY `InvoiceGaji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kinerja`
--
ALTER TABLE `kinerja`
  ADD CONSTRAINT `kinerja_ibfk_1` FOREIGN KEY (`NIK`) REFERENCES `user` (`NIK`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kinerja_ibfk_2` FOREIGN KEY (`KodeJobdesk`) REFERENCES `jobdesk` (`KodeJobdesk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`IdJabatan`) REFERENCES `jabatan` (`IdJabatan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
