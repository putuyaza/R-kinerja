<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller ResetController
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Reset extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper(array('form', 'url'));

    $this->load->library('form_validation');
  }

  public function getresset()
  {
    $this->load->view('reset');
  }


  public function resetpassword(){
    $this->load->helper(array('form', 'url'));

    $this->load->library('form_validation');
    $this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email',
    array(
               'required'      => 'Email tidak boleh kosong %s.',
               'valid_email'     => 'Gunakan email yang valid'
       )
  );
        if ($this->form_validation->run() == FALSE)
        {
                              $this->load->view('reset');
        }else
        {
          $email = $this->input->post('Email');
          $user = $this->db->get_where('user',['Email'=>$email])->row_array();
                if($user)
                {
                  $token = base64_encode(random_bytes(32));
                  $userToken =
                  [
                    'Email' => $email,
                    'Token'=>$token,
                    'Date_created' => time()
                  ];
                  $this->db->insert('user_token', $userToken);
                  $this->_sendEmail($token,'forgot');
                  $this->session->set_flashdata('sukses','Mohon untuk mengecek email anda');
                  redirect('reset/getresset');
                }else
                {
                  $this->session->set_flashdata('msg','Email anda belum terdaftar');
                  redirect('reset/getresset');
                }
          }

  }

  private function _sendEmail($token,$type){
    $config = [
      'protocol'  => 'smtp',
      'smtp_host' => 'ssl://smtp.googlemail.com',
      'smtp_user' => 'reportkinerja.harmonipermata@gmail.com',
      'smtp_pass' => 'Batulablab1',
      'smtp_port' => 465,
      'mailtype'  => 'html',
      'charset'   => 'utf-8',
      'newline'   => "\r\n"
    ];
    $this->load->library('email', $config);
    $this->email->initialize($config);
    $this->email->from('reportkinerja.harmonipermata@gmail.com', 'Report Kinerja');
    $this->email->to($this->input->post('Email'));
        if($type=='forgot'){
            $this->email->subject('Reset Password');
          $this->email->message('klik reset untuk reset password anda : <a link href="'.base_url(). 'reset/gantipassword?email='.$this->input->post('Email').'&token='.urlencode($token). '">Reset Password</a>');
        }

      if($this->email->send()){
        return true;
      }else{
        echo $this->email->print_debugger();
        die;
      }

  }

  public function gantipassword(){
    $email = $this->input->get('email');
    $token = $this->input->get('token');
  $user =  $this->db->get_where('user',['Email'=>$email])->row_array();
    if($user){
      $userToken = $this->db->get_where('user_token', ['Token'=>$token])->row_array();
      if($userToken){
      $this->session->set_userdata('resetEmail',$email);
      $this->getgantipassword();
      }else{
        $this->session->set_flashdata('msg','Maaf Token anda tidak ada');
        redirect('reset/getresset');
      }

    }else{
      $this->session->set_flashdata('msg','Reset Password gagal');
      redirect('reset/getresset');
    }
  }

  public function getgantipassword(){
    if(!$this->session->userdata('resetEmail')){
      redirect('login');
    }
    $this->form_validation->set_rules('Password','Password','required|min_length[5]'
    ,
    array(
               'required'      => 'Form tidak boleh kosong %s.',
               'min_length'     => 'Password Minimal 5 karakter'
       )

    ); // min_length[5] password tidak boleh kurang dari lima
    $this->form_validation->set_rules('Passwordconf','Retype Password','required|matches[Password]',
    array(
               'required'      => 'Form tidak boleh kosong %s.',
               'matches'     => 'Konfirmasi Password anda tidak cocok'
       )
    );
    $this->form_validation->set_rules('Username','Username','required|min_length[5]|max_length[20]',
    array(
               'required'      => 'Form  Username tidak boleh kosong %s.',
               'min_length'     => 'Username Minimal 5 karakter',
               'max_length'     => 'Username Maksimal 20 karakter'
       )

    );
    if ($this->form_validation->run() == FALSE)
    {
          $this->load->view('gantipassword');
    }else
    {
    $password = base64_encode($this->input->post('Password'));
    $username = $this->input->post('Username');
    $email = $this->session->userdata('resetEmail');
    $this->db->set('Password',$password);
    $this->db->set('Username',$username);
    $this->db->where('Email', $email);
    $this->db->update('user');
    $this->session->unset_userdata('resetEmail');
    $this->session->set_flashdata('sukses','Password dan username anda berhasil reset, silakan login');
    redirect('login');
    }
  }

}


/* End of file ResetController.php */
/* Location: ./application/controllers/ResetController.php */
