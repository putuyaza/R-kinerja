<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller Dasboard
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Dasboard extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Register_model');
    $this->load->model('Datakaryawan');
    $this->load->model('Jobdesk_model');
    $this->load->helper('rupiah_helper');

  }

  public function admin()
  {
    $data['countkaryawan'] = $this->Datakaryawan->countkaryawan();
    $data['countadmin'] = $this->Datakaryawan->countadmin();
    $data['countmanager'] = $this->Datakaryawan->countmanager();
    $data['countbod1'] = $this->Datakaryawan->countbod1();
    $this->load->view('admin/dasboardAdmin',$data);
  }

  public function karyawan()
  {
    $data['countkaryawan'] = $this->Datakaryawan->countkaryawan();
    $data['countadmin'] = $this->Datakaryawan->countadmin();
    $data['countmanager'] = $this->Datakaryawan->countmanager();
    $data['countbod1'] = $this->Datakaryawan->countbod1();
    $this->load->view('karyawan/dasboardKaryawan',$data);
  }
  public function CEO()
  {
    $data['countkaryawan'] = $this->Datakaryawan->countkaryawan();
    $data['countadmin'] = $this->Datakaryawan->countadmin();
    $data['countmanager'] = $this->Datakaryawan->countmanager();
    $data['countbod1'] = $this->Datakaryawan->countbod1();
    $this->load->view('CEO/dasboardCEO',$data);
   }

  public function manager()
  {
    $data['countkaryawan'] = $this->Datakaryawan->countkaryawan();
    $data['countadmin'] = $this->Datakaryawan->countadmin();
    $data['countmanager'] = $this->Datakaryawan->countmanager();
    $data['countbod1'] = $this->Datakaryawan->countbod1();
    $this->load->view('manager/dasboardManager',$data);
  }

 public function registerform(){
  $hasil = $this->Register_model->getAll();
  $data['hasil'] = $hasil; // menampung di variable $data
   $this->load->View('admin/registernew',$data);
 }

public function viewUser(){
  $data = $this->Datakaryawan->getAll();
  $hasil['data'] = $data; // menampung semua data ke variable
    $this->load->view('admin/viewUser',$hasil);

}


 public function inputjobdesk($NIK){
   $hasil = $this->Jobdesk_model->getperjob($NIK);
   $data['sumkinerja'] =$this->Jobdesk_model->getsum($NIK);
   $data['hasil'] = $hasil; // menampung di variable $data
   $this->load->view('karyawan/inputJobdesk',$data);
 }
 public function inputjobdeskadmin($NIK){
   $hasil = $this->Jobdesk_model->getperjob($NIK);
   $data['sumkinerja'] =$this->Jobdesk_model->getsum($NIK);
   $data['hasil'] = $hasil; // menampung di variable $data
   $this->load->view('admin/inputJobdeskAdmin',$data);
 }

 public function deleteuser($NIK){
   if($NIK==""){
     $this->session->set_flashdata('gagal',"Data Anda Gagal Di Hapus");
     redirect('Dasboard/viewUser');
   }else{
     $this->db->where('NIK', $NIK);
     $this->db->delete('user');
     $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
     redirect('Dasboard/viewUser');
   }
 }

public function edituser($NIK){
  $hasil = $this->Register_model->getperuser($NIK);
  $data['hasil'] = $hasil; // menampung di variable $data
  $this->load->view('edituser',$data);

}

public function viewkinerjaperkaryawan($NIK){
  $hasil = $this->Jobdesk_model->getjobperkaryawan($NIK);
  $data['sumkinerja'] =$this->Jobdesk_model->getsumkinerja($NIK);
  $data['hasil'] = $hasil; // menampung di variable $data
  $this->load->view('karyawan/viewkinerja',$data);

}

public function laporankinerja(){
  $data['hasil'] = $this->Jobdesk_model->getalljob();
  $data['controller'] = $this;
  // $data['sumkinerja'] =  $this->Jobdesk_model->getsumjob();
  $this->load->view('admin/laporankinerja',$data);
}

public function cekgaji($NIK){
  $sql=" SELECT sum(NIK) AS NIK  FROM kinerja WHERE NIK='$NIK'";
    $data = $this->db->query($sql);
    return $data->row()->NIK;
}


public function sumdata($NIK){
  $sql=" SELECT sum(EstimasiKerja) AS EstimasiKerja FROM jobdesk WHERE NIK='$NIK'";
    $data = $this->db->query($sql);
    return $data->row()->EstimasiKerja;
}

public function deletkinerja($NIK){
  $this->db->where('NIK', $NIK);
  $this->db->delete('jobdesk');
  $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
  redirect('Dasboard/laporankinerja');
}

public function laporangaji(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->view('admin/laporangaji',$data);
}



// Manager
public function viewUserManager(){
  $data = $this->Datakaryawan->getAll();
  $hasil['data'] = $data; // menampung semua data ke variable
    $this->load->view('manager/viewUser',$hasil);

}
public function deleteuserfrommanager($NIK){
  if($NIK==""){
    $this->session->set_flashdata('gagal',"Data Anda Gagal Di Hapus");
    redirect('Dasboard/viewUserManager');
  }else{
    $this->db->where('NIK', $NIK);
    $this->db->delete('user');
    $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
    redirect('Dasboard/viewUserManager');
  }
}

public function laporankinerjafrommanager(){
  $data['hasil'] = $this->Jobdesk_model->getalljob();
  $data['controller'] = $this;
  // $data['sumkinerja'] =  $this->Jobdesk_model->getsumjob();
  $this->load->view('manager/laporankinerja',$data);
}

public function deletkinerjafrommanager($NIK){
  $this->db->where('NIK', $NIK);
  $this->db->delete('jobdesk');
  $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
  redirect('Dasboard/laporankinerjafrommanager');
}

public function laporangajifrommanager(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->view('manager/laporangaji',$data);
}

public function registerformfrommanager(){
 $hasil = $this->Register_model->getAll();
 $data['hasil'] = $hasil; // menampung di variable $data
  $this->load->View('manager/registernew',$data);
}
public function slipgajifrommanager(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->library('pdf');
  $this->pdf->setPaper('A5', 'potrait');
  $this->pdf->filename = "laporan gaji.pdf";
  $this->pdf->load_view('manager/slipgaji',$data);
}
public function exportlaporangajifrommanager(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->view('manager/printlaporangaji',$data);
}

public function laporan_pdffrommanager(){

    $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
    $data['controller'] = $this;
    $this->load->library('pdf');
    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->filename = "laporan gaji.pdf";
    $this->pdf->load_view('manager/printpdfgaji',$data);


}


// akhir manager

// cEO
public function viewUserCEO(){
  $data = $this->Datakaryawan->getAll();
  $hasil['data'] = $data; // menampung semua data ke variable
    $this->load->view('CEO/viewUser',$hasil);

}
public function deleteuserfromceo($NIK){
  if($NIK==""){
    $this->session->set_flashdata('gagal',"Data Anda Gagal Di Hapus");
    redirect('Dasboard/viewUserCEO');
  }else{
    $this->db->where('NIK', $NIK);
    $this->db->delete('user');
    $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
    redirect('Dasboard/viewUserCEO');
  }
}

public function laporankinerjafromceo(){
  $data['hasil'] = $this->Jobdesk_model->getalljob();
  $data['controller'] = $this;
  // $data['sumkinerja'] =  $this->Jobdesk_model->getsumjob();
  $this->load->view('CEO/laporankinerja',$data);
}

public function deletkinerjafromceo($NIK){
  $this->db->where('NIK', $NIK);
  $this->db->delete('jobdesk');
  $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
  redirect('Dasboard/laporankinerjafromceo');
}

public function laporangajifromceo(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->view('CEO/laporangaji',$data);
}

public function registerformfromceo(){
 $hasil = $this->Register_model->getAll();
 $data['hasil'] = $hasil; // menampung di variable $data
  $this->load->View('CEO/registernew',$data);
}

public function slipgajifromceo(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->library('pdf');
  $this->pdf->setPaper('A5', 'potrait');
  $this->pdf->filename = "laporan gaji.pdf";
  $this->pdf->load_view('CEO/slipgaji',$data);
}
public function exportlaporangajifromceo(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->view('CEO/printlaporangaji',$data);
}
public function laporan_pdffromceo(){

    $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
    $data['controller'] = $this;
    $this->load->library('pdf');
    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->filename = "laporan gaji.pdf";
    $this->pdf->load_view('CEO/printpdfgaji',$data);


}

// Akhir CEO
public function laporan_pdf(){

    $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
    $data['controller'] = $this;
    $this->load->library('pdf');
    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->filename = "laporan gaji.pdf";
    $this->pdf->load_view('admin/printpdfgaji',$data);


}

public function exportlaporangaji(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->view('admin/printlaporangaji',$data);
}

public function slipgaji(){
  $data['hasilgaji'] = $this->Jobdesk_model->getgaji();
  $data['controller'] = $this;
  $this->load->library('pdf');
  $this->pdf->setPaper('A5', 'potrait');
  $this->pdf->filename = "laporan gaji.pdf";
  $this->pdf->load_view('admin/slipgaji',$data);
}

}


/* End of file Dasboard.php */
/* Location: ./application/controllers/Dasboard.php */
