<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller Eror403
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Eror403 extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->view('error403');
  }

}


/* End of file Eror403.php */
/* Location: ./application/controllers/Eror403.php */