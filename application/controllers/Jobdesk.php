<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobdesk extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Jobdesk_model');
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');

  }
// function tambah untuk karyawan
  function tambah()
  {
    if(isset($_POST['addjob'])){

      $datajobdesk = [
        'NIK'=>$this->input->post('NIK'),
        'Jobdesk'=>$this->input->post('Jobdesk'),
        'EstimasiKerja'=>$this->input->post('Jam'),
        'Tanggal'=> date('Y-m-d')
      ];

      $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
      array(
                 'required'      => 'Form NIK tidak boleh kosong .',
                 'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
                 'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
                 'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
         )
      );
      $this->form_validation->set_rules('Jobdesk','Jobdesk','required',
      array(
                 'required'      => 'Form %s tidak boleh kosong '

         )
      );

      $this->form_validation->set_rules('Jam','Estimasi Kerja','required|max_length[3]|numeric',
      array(
                 'required'      => 'Form NIK tidak boleh kosong %s.',
                 'max_length'     => 'Maksimal 3 digit angka untuk pengisian Estimasi.',
                 'numeric'     => 'Estimasi tidak valid , Estimasi harus menggunakan angka'
         )
      );

      if ($this->form_validation->run()==FALSE){
        $this->load->view('karyawan/errorjobdesk');
        }else {
          $data = $this->Jobdesk_model->tambah($datajobdesk);
            if($data){
              $this->session->set_flashdata('sukses','Jobdesk berhasil disimpan');

              redirect('Dasboard/inputjobdesk/'.$this->session->userdata('NIK'));

            }else{
              $this->session->set_flashdata('gagal','Data Gagal  Disimpan');

              redirect('Dasboard/inputjobdesk/'.$this->session->userdata('NIK'));
            }

    }
  }

}
public function updatejobperkaryawan($KodeJobdesk){
  if(isset($_POST['update'])){
  $datajobdesk = [
    'NIK'=>$this->input->post('NIK'),
    'Jobdesk'=>$this->input->post('Jobdesk'),
    'EstimasiKerja'=>$this->input->post('Jam'),
    'Tanggal'=> $this->input->post('TglJobdesk')
  ];

  $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
  array(
             'required'      => 'Form %s tidak boleh kosong .',
             'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
             'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
             'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
     )
  );
  $this->form_validation->set_rules('Jobdesk','Jobdesk','required',
  array(
             'required'      => 'Form %s tidak boleh kosong '

     )
  );

  $this->form_validation->set_rules('Jam','Estimasi Kerja','required|max_length[3]|numeric',
  array(
             'required'      => 'Form %s tidak boleh kosong %s.',
             'max_length'     => 'Maksimal 3 digit angka untuk pengisian Estimasi.',
             'numeric'     => 'Estimasi tidak valid , Estimasi harus menggunakan angka'
     )
  );

  if ($this->form_validation->run()==FALSE){
    $this->load->view('karyawan/errorjobdesk');
    }else {
      $data = $this->Jobdesk_model->update($datajobdesk,$KodeJobdesk);
        if($data){
          $this->session->set_flashdata('sukses','Jobdesk berhasil Diupdate');

          redirect('Dasboard/viewkinerjaperkaryawan/'.$this->session->userdata('NIK'));

        }else{
          $this->session->set_flashdata('gagal','Data Gagal  Diupdate');

          redirect('Dasboard/viewkinerjaperkaryawan/'.$this->session->userdata('NIK'));
        }

      }

}
}

public function deletejobperkaryawan($KodeJobdesk){
  $this->db->where('KodeJobdesk',$KodeJobdesk);
  $this->db->delete('jobdesk');
  $this->session->set_flashdata('sukses',"Jobdesk Berhasil Dihapus");
  redirect('Dasboard/viewkinerjaperkaryawan/'.$this->session->userdata('NIK'));
}
// akhir function tambah untuk karyawan

public function update($KodeJobdesk){
  if(isset($_POST['update'])){
  $datajobdesk = [
    'NIK'=>$this->input->post('NIK'),
    'Jobdesk'=>$this->input->post('Jobdesk'),
    'EstimasiKerja'=>$this->input->post('Jam'),
    'Tanggal'=> $this->input->post('TglJobdesk')
  ];

  $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
  array(
             'required'      => 'Form %s tidak boleh kosong .',
             'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
             'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
             'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
     )
  );
  $this->form_validation->set_rules('Jobdesk','Jobdesk','required',
  array(
             'required'      => 'Form %s tidak boleh kosong '

     )
  );

  $this->form_validation->set_rules('Jam','Estimasi Kerja','required|max_length[3]|numeric',
  array(
             'required'      => 'Form %s tidak boleh kosong %s.',
             'max_length'     => 'Maksimal 3 digit angka untuk pengisian Estimasi.',
             'numeric'     => 'Estimasi tidak valid , Estimasi harus menggunakan angka'
     )
  );

  if ($this->form_validation->run()==FALSE){
    $this->load->view('karyawan/errorjobdesk');
    }else {
      $data = $this->Jobdesk_model->update($datajobdesk,$KodeJobdesk);
        if($data){
          $this->session->set_flashdata('sukses','Jobdesk berhasil Diupdate');

          redirect('Dasboard/inputjobdesk/'.$this->session->userdata('NIK'));

        }else{
          $this->session->set_flashdata('gagal','Data Gagal  Diupdate');

          redirect('Dasboard/inputjobdesk/'.$this->session->userdata('NIK'));
        }

      }

}

}

public function delete($KodeJobdesk){
  $this->db->where('KodeJobdesk',$KodeJobdesk);
  $this->db->delete('jobdesk');
  $this->session->set_flashdata('sukses',"Jobdesk Berhasil Dihapus");
  redirect('Dasboard/inputjobdesk/'.$this->session->userdata('NIK'));
}
// akhir function tambah untuk karyawan

// function tambah untuk admin
function tambahbyadmin()
{
  if(isset($_POST['addjob'])){

    $datajobdesk = [
      'NIK'=>$this->input->post('NIK'),
      'Jobdesk'=>$this->input->post('Jobdesk'),
      'EstimasiKerja'=>$this->input->post('Jam'),
      'Tanggal'=> date('Y-m-d')
    ];

    $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
    array(
               'required'      => 'Form NIK tidak boleh kosong .',
               'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
               'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
               'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
       )
    );
    $this->form_validation->set_rules('Jobdesk','Jobdesk','required',
    array(
               'required'      => 'Form %s tidak boleh kosong '

       )
    );

    $this->form_validation->set_rules('Jam','Estimasi Kerja','required|max_length[3]|numeric',
    array(
               'required'      => 'Form NIK tidak boleh kosong %s.',
               'max_length'     => 'Maksimal 3 digit angka untuk pengisian Estimasi.',
               'numeric'     => 'Estimasi tidak valid , Estimasi harus menggunakan angka'
       )
    );

    if ($this->form_validation->run()==FALSE){
      $this->load->view('admin/errorjobdesk');
      }else {
        $data = $this->Jobdesk_model->tambah($datajobdesk);
          if($data){
            $this->session->set_flashdata('sukses','Jobdesk berhasil disimpan');

            redirect('Dasboard/inputjobdeskadmin/'.$this->session->userdata('NIK'));

          }else{
            $this->session->set_flashdata('gagal','Data Gagal  Disimpan');

            redirect('Dasboard/inputjobdeskadmin/'.$this->session->userdata('NIK'));
          }

  }
}

}

public function updatebyadmin($KodeJobdesk){
if(isset($_POST['update'])){
$datajobdesk = [
  'NIK'=>$this->input->post('NIK'),
  'Jobdesk'=>$this->input->post('Jobdesk'),
  'EstimasiKerja'=>$this->input->post('Jam'),
  'Tanggal'=> $this->input->post('TglJobdesk')
];

$this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
array(
           'required'      => 'Form %s tidak boleh kosong .',
           'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
           'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
           'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
   )
);
$this->form_validation->set_rules('Jobdesk','Jobdesk','required',
array(
           'required'      => 'Form %s tidak boleh kosong '

   )
);

$this->form_validation->set_rules('Jam','Estimasi Kerja','required|max_length[3]|numeric',
array(
           'required'      => 'Form %s tidak boleh kosong %s.',
           'max_length'     => 'Maksimal 3 digit angka untuk pengisian Estimasi.',
           'numeric'     => 'Estimasi tidak valid , Estimasi harus menggunakan angka'
   )
);

if ($this->form_validation->run()==FALSE){
  $this->load->view('admin/errorjobdesk');
  }else {
    $data = $this->Jobdesk_model->update($datajobdesk,$KodeJobdesk);
      if($data){
        $this->session->set_flashdata('sukses','Jobdesk berhasil Diupdate');

        redirect('Dasboard/inputjobdeskadmin/'.$this->session->userdata('NIK'));

      }else{
        $this->session->set_flashdata('gagal','Data Gagal  Diupdate');

        redirect('Dasboard/inputjobdeskadmin/'.$this->session->userdata('NIK'));
      }

    }

}

}

public function deletebyadmin($KodeJobdesk){
$this->db->where('KodeJobdesk',$KodeJobdesk);
$this->db->delete('jobdesk');
$this->session->set_flashdata('sukses',"Jobdesk Berhasil Dihapus");
redirect('Dasboard/inputjobdeskadmin/'.$this->session->userdata('NIK'));
}
// akhir function tambah untuk admin

public function tambahkinerja($NIK){
  $gajikinerja= [
    'Gaji' => $this->input->post('Gaji'),
    'Bonus' => $this->input->post('Bonus'),
    'NIK' => $this->input->post('NIK'),
    'Status'=> 1
  ];

  $data = $this->Jobdesk_model->tambahkinerja($NIK,$gajikinerja);
  if($data){
    $this->session->set_flashdata('sukses','Gaji Berhasil di bayar');

    redirect('Dasboard/laporangaji');
  }else{
    $this->session->set_flashdata('gagal','Gaji gagal di bayar');

    redirect('Dasboard/laporangaji');
  }

}

public function posting($NIK){
  $datakinerja = [
    'Gaji'=>0,
    'Bonus'=>0,
    'NIK'=>$NIK,
    'Status'=>0
  ];

  $data = $this->Jobdesk_model->posting($datakinerja);
  if($data){
    $this->session->set_flashdata('sukses','Gaji berhasil diposting pada laporan gaji');
    redirect('Dasboard/laporankinerja');
  }else{
    $this->session->set_flashdata('Gagal','Gaji gagal diposting pada laporan gaji');
    redirect('Dasboard/laporankinerja');
  }
}

public function deleteposting($NIK){
  $this->db->where('NIK',$NIK);
  $this->db->delete('kinerja');
  $this->session->set_flashdata('sukses',"Gaji Berhasil di unposting pada laporan gaji");
  redirect('Dasboard/laporankinerja');
}

//Manager
public function postingfrommanager($NIK){
  $datakinerja = [
    'Gaji'=>0,
    'Bonus'=>0,
    'NIK'=>$NIK,
    'Status'=>0
  ];

  $data = $this->Jobdesk_model->posting($datakinerja);
  if($data){
    $this->session->set_flashdata('sukses','Gaji berhasil diposting pada laporan gaji');
    redirect('Dasboard/laporankinerjafrommanager');
  }else{
    $this->session->set_flashdata('Gagal','Gaji gagal diposting pada laporan gaji');
    redirect('Dasboard/laporankinerjafrommanager');
  }
}

public function deletepostingfrommanager($NIK){
  $this->db->where('NIK',$NIK);
  $this->db->delete('kinerja');
  $this->session->set_flashdata('sukses',"Gaji Berhasil di unposting pada laporan gaji");
  redirect('Dasboard/laporankinerjafrommanager');
}


public function tambahkinerjafrommanager($NIK){
  $gajikinerja= [
    'Gaji' => $this->input->post('Gaji'),
    'Bonus' => $this->input->post('Bonus'),
    'NIK' => $this->input->post('NIK'),
    'Status'=> 1
  ];

  $data = $this->Jobdesk_model->tambahkinerja($NIK,$gajikinerja);
  if($data){
    $this->session->set_flashdata('sukses','Gaji Berhasil di bayar');

    redirect('Dasboard/laporangajifrommanager');
  }else{
    $this->session->set_flashdata('gagal','Gaji gagal di bayar');

    redirect('Dasboard/laporangajifrommanager');
  }

}
//manager

//ceo
public function postingfromceo($NIK){
  $datakinerja = [
    'Gaji'=>0,
    'Bonus'=>0,
    'NIK'=>$NIK,
    'Status'=>0
  ];

  $data = $this->Jobdesk_model->posting($datakinerja);
  if($data){
    $this->session->set_flashdata('sukses','Gaji berhasil diposting pada laporan gaji');
    redirect('Dasboard/laporankinerjafromceo');
  }else{
    $this->session->set_flashdata('Gagal','Gaji gagal diposting pada laporan gaji');
    redirect('Dasboard/laporankinerjafromceo');
  }
}

public function deletepostingfromceo($NIK){
  $this->db->where('NIK',$NIK);
  $this->db->delete('kinerja');
  $this->session->set_flashdata('sukses',"Gaji Berhasil di unposting pada laporan gaji");
  redirect('Dasboard/laporankinerjafromceo');
}


public function tambahkinerjafromceo($NIK){
  $gajikinerja= [
    'Gaji' => $this->input->post('Gaji'),
    'Bonus' => $this->input->post('Bonus'),
    'NIK' => $this->input->post('NIK'),
    'Status'=> 1
  ];

  $data = $this->Jobdesk_model->tambahkinerja($NIK,$gajikinerja);
  if($data){
    $this->session->set_flashdata('sukses','Gaji Berhasil di bayar');

    redirect('Dasboard/laporangajifromceo');
  }else{
    $this->session->set_flashdata('gagal','Gaji gagal di bayar');

    redirect('Dasboard/laporangajifromceo');
  }

}
// akhir ceo


}
