<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller Register
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Register extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Register_model');
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
  }

  public function postdata()
  {

    if(isset($_POST)){

    $dataregister = array(
      'NIK' => $this->input->post('NIK'),
      'IdJabatan' => $this->input->post('Jabatan'),
      'Nama' => $this->input->post('Nama'),
      'Username' => $this->input->post('Username'),
      'Password' => base64_encode($this->input->post('Password')),
      'Level' => $this->input->post('Level'),
      'JenisKelamin' => $this->input->post('JenisKelamin'),
      'NoTelpon' => $this->input->post('NoTelpon'),
      'Email' => $this->input->post('Email'),
      'TargetJam' =>$this->input->post('TargetJam'),
      'TglLahir' => $this->input->post('TglLahir'),
      'Status' => $this->input->post('Status')


    );




    $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|is_unique[user.NIK]|numeric',
    array(
               'required'      => 'Form NIK tidak boleh kosong %s.',
               'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
               'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
               'is_unique'     => 'NIK Sudah dipakai',
               'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
       )

    );
    $this->form_validation->set_rules('Username','Username','required|min_length[5]|max_length[20]|is_unique[user.Username]',
    array(
               'required'      => 'Form  Username tidak boleh kosong %s.',
               'min_length'     => 'Username Minimal 5 karakter',
               'max_length'     => 'Username Maksimal 20 karakter',
               'is_unique'     => 'Username Sudah dipakai'
       )

    );
    $this->form_validation->set_rules('Email','Email','required|valid_email|is_unique[user.Email]',
    array(
               'required'      => 'Form Email tidak boleh kosong %s.',
               'valid_email'     => 'email anda tidak valid',
               'is_unique'     => 'Email Sudah dipakai'
       )
    );
    $this->form_validation->set_rules('Password','Password','required|min_length[5]'
    ,
    array(
               'required'      => 'Form tidak boleh kosong %s.',
               'min_length'     => 'Password Minimal 5 karakter'
       )

    ); // min_length[5] password tidak boleh kurang dari lima
    $this->form_validation->set_rules('Passwordconf','Retype Password','required|matches[Password]',
    array(
               'required'      => 'Form tidak boleh kosong %s.',
               'matches'     => 'Konfirmasi Password anda tidak cocok'
       )

    ); // matches[password] mencocokan password
    $this->form_validation->set_rules('TargetJam','Target Jam','required|numeric',
    array(
               'required'      => 'Form Target Jam tidak boleh kosong %s.',
               'numeric'     => 'Input target jam tidak diperbolehkan dengan huruf, harus angka'

       )

    );
    $this->form_validation->set_rules('NoTelpon','No Telpon','required|numeric',
    array(
      'required'      => 'Form Target Jam tidak boleh kosong %s.',
      'numeric'     => 'No telpon tidak valid'
       )
       );

    if ($this->form_validation->run()==FALSE){
      $this->load->view('admin/registereror');
  }

  else {
    $data = $this->Register_model->simpandata($dataregister);
    if($data){
     $this->session->set_flashdata('sukses','Data Berhasil Disimpan');

     redirect('Dasboard/registerform');

    }else{
      $this->session->set_flashdata('gagal','Data Gagal Disimpan');

      redirect('Dasboard/registerform');
    }


  }
  }
  }

  public function updateuser($NIK){

      if(isset($_POST)){

      $dataregister = array(
        'NIK' => $this->input->post('NIK'),
        'IdJabatan' => $this->input->post('Jabatan'),
        'Nama' => $this->input->post('Nama'),
        'JenisKelamin' => $this->input->post('JenisKelamin'),
        'Level' => $this->input->post('Level'),
        'NoTelpon' => $this->input->post('NoTelpon'),
        'Email' => $this->input->post('Email'),
        'TargetJam' =>$this->input->post('TargetJam'),
        'TglLahir' => $this->input->post('TglLahir'),
        'Status' => $this->input->post('Status')


      );




      $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
      array(
                 'required'      => 'Form NIK tidak boleh kosong %s.',
                 'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
                 'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
                 'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
         )

      );

      $this->form_validation->set_rules('TargetJam','Target Jam','required|numeric',
      array(
        'required'      => 'Form Target Jam tidak boleh kosong %s.',
        'numeric'     => 'Input target jam tidak diperbolehkan dengan huruf,harus angka'

         )

      );
      $this->form_validation->set_rules('NoTelpon','No Telpon','required|numeric',
      array(
        'required'      => 'Form Target Jam tidak boleh kosong %s.',
        'numeric'     => 'No telpon tidak valid'

         )

      );


      $this->form_validation->set_rules('Email','Email','required|valid_email',
      array(
                 'required'      => 'Form Email tidak boleh kosong %s.',
                 'valid_email'     => 'email anda tidak valid'
         )
      );


      if ($this->form_validation->run()==FALSE){
        $this->load->view('admin/registererorupdate');
    }

    else {
      $data = $this->Register_model->updateuser($dataregister,$NIK);
      if($data){
       $this->session->set_flashdata('sukses','Data Berhasil Di Update');

       redirect('Dasboard/viewUser');

      }else{
        $this->session->set_flashdata('gagal','Data Gagal  Disimpan');

        redirect('Dasboard/viewUser');
      }



    }

  }
}

public function updateperuser($NIK){
  if(isset($_POST)){

  $dataregister = array(
    'NIK' => $this->input->post('NIK'),
    'IdJabatan' => $this->input->post('Jabatan'),
    'Nama' => $this->input->post('Nama'),
    'Username' => $this->input->post('Username'),
    'Password' => base64_encode($this->input->post('Password')),
    'Level' => $this->input->post('Level'),
    'JenisKelamin' => $this->input->post('JenisKelamin'),
    'NoTelpon' => $this->input->post('NoTelpon'),
    'Email' => $this->input->post('Email'),
    'TargetJam' =>$this->input->post('TargetJam'),
    'TglLahir' => $this->input->post('TglLahir'),
    'Status' => $this->input->post('Status')


  );




  $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
  array(
             'required'      => 'Form NIK tidak boleh kosong %s.',
             'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
             'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
             'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
     )

  );
  $this->form_validation->set_rules('Username','Username','required|min_length[5]|max_length[20]',
  array(
             'required'      => 'Form  Username tidak boleh kosong %s.',
             'min_length'     => 'Username Minimal 5 karakter',
             'max_length'     => 'Username Maksimal 20 karakter'
     )

  );
  $this->form_validation->set_rules('Email','Email','required|valid_email',
  array(
             'required'      => 'Form Email tidak boleh kosong %s.',
             'valid_email'     => 'email anda tidak valid',
             'is_unique'     => 'Email Sudah dipakai'
     )
  );
  $this->form_validation->set_rules('Password','Password','required|min_length[5]'
  ,
  array(
             'required'      => 'Form tidak boleh kosong %s.',
             'min_length'     => 'Password Minimal 5 karakter'
     )

  ); // min_length[5] password tidak boleh kurang dari lima
  $this->form_validation->set_rules('Passwordconf','Retype Password','required|matches[Password]',
  array(
             'required'      => 'Form tidak boleh kosong %s.',
             'matches'     => 'Konfirmasi Password anda tidak cocok'
     )

  ); // matches[password] mencocokan password
  $this->form_validation->set_rules('TargetJam','Target Jam','required|numeric',
  array(
             'required'      => 'Form Target Jam tidak boleh kosong %s.',
             'numeric'     => 'Input target jam tidak diperbolehkan dengan huruf, harus angka'

     )

  );
  $this->form_validation->set_rules('NoTelpon','No Telpon','required|numeric',
  array(
    'required'      => 'Form Target Jam tidak boleh kosong %s.',
    'numeric'     => 'No telpon tidak valid'
     )
     );

  if ($this->form_validation->run()==FALSE){
    $this->load->view('registererorupdate');
}

else {
  $data = $this->Register_model->updateperuser($dataregister,$NIK);
  if($data){
   $this->session->set_flashdata('sukses','Profil Berhasil Diupdate');

   redirect('Dasboard/edituser/'.$NIK);

  }else{
    $this->session->set_flashdata('gagal','Profil Gagal Diupdate');

    redirect('Dasboard/edituser/'.$NIK);
  }


}
}

}
// Manager
public function updateuserfrommanager($NIK){

    if(isset($_POST)){

    $dataregister = array(
      'NIK' => $this->input->post('NIK'),
      'IdJabatan' => $this->input->post('Jabatan'),
      'Nama' => $this->input->post('Nama'),
      'JenisKelamin' => $this->input->post('JenisKelamin'),
      'Level' => $this->input->post('Level'),
      'NoTelpon' => $this->input->post('NoTelpon'),
      'Email' => $this->input->post('Email'),
      'TargetJam' =>$this->input->post('TargetJam'),
      'TglLahir' => $this->input->post('TglLahir'),
      'Status' => $this->input->post('Status')


    );




    $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
    array(
               'required'      => 'Form NIK tidak boleh kosong %s.',
               'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
               'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
               'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
       )

    );

    $this->form_validation->set_rules('TargetJam','Target Jam','required|numeric',
    array(
      'required'      => 'Form Target Jam tidak boleh kosong %s.',
      'numeric'     => 'Input target jam tidak diperbolehkan dengan huruf,harus angka'

       )

    );
    $this->form_validation->set_rules('NoTelpon','No Telpon','required|numeric',
    array(
      'required'      => 'Form Target Jam tidak boleh kosong %s.',
      'numeric'     => 'No telpon tidak valid'

       )

    );


    $this->form_validation->set_rules('Email','Email','required|valid_email',
    array(
               'required'      => 'Form Email tidak boleh kosong %s.',
               'valid_email'     => 'email anda tidak valid'
       )
    );


    if ($this->form_validation->run()==FALSE){
      $this->load->view('manager/registererorupdate');
  }

  else {
    $data = $this->Register_model->updateuser($dataregister,$NIK);
    if($data){
     $this->session->set_flashdata('sukses','Data Berhasil Di Update');

     redirect('Dasboard/viewUserManager');

    }else{
      $this->session->set_flashdata('gagal','Data Gagal  Disimpan');

      redirect('Dasboard/viewUserManager');
    }



  }

}
}

public function postdatafrommanager()
{

  if(isset($_POST)){

  $dataregister = array(
    'NIK' => $this->input->post('NIK'),
    'IdJabatan' => $this->input->post('Jabatan'),
    'Nama' => $this->input->post('Nama'),
    'Username' => $this->input->post('Username'),
    'Password' => base64_encode($this->input->post('Password')),
    'Level' => $this->input->post('Level'),
    'JenisKelamin' => $this->input->post('JenisKelamin'),
    'NoTelpon' => $this->input->post('NoTelpon'),
    'Email' => $this->input->post('Email'),
    'TargetJam' =>$this->input->post('TargetJam'),
    'TglLahir' => $this->input->post('TglLahir'),
    'Status' => $this->input->post('Status')


  );




  $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|is_unique[user.NIK]|numeric',
  array(
             'required'      => 'Form NIK tidak boleh kosong %s.',
             'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
             'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
             'is_unique'     => 'NIK Sudah dipakai',
             'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
     )

  );
  $this->form_validation->set_rules('Username','Username','required|min_length[5]|max_length[20]|is_unique[user.Username]',
  array(
             'required'      => 'Form  Username tidak boleh kosong %s.',
             'min_length'     => 'Username Minimal 5 karakter',
             'max_length'     => 'Username Maksimal 20 karakter',
             'is_unique'     => 'Username Sudah dipakai'
     )

  );
  $this->form_validation->set_rules('Email','Email','required|valid_email|is_unique[user.Email]',
  array(
             'required'      => 'Form Email tidak boleh kosong %s.',
             'valid_email'     => 'email anda tidak valid',
             'is_unique'     => 'Email Sudah dipakai'
     )
  );
  $this->form_validation->set_rules('Password','Password','required|min_length[5]'
  ,
  array(
             'required'      => 'Form tidak boleh kosong %s.',
             'min_length'     => 'Password Minimal 5 karakter'
     )

  ); // min_length[5] password tidak boleh kurang dari lima
  $this->form_validation->set_rules('Passwordconf','Retype Password','required|matches[Password]',
  array(
             'required'      => 'Form tidak boleh kosong %s.',
             'matches'     => 'Konfirmasi Password anda tidak cocok'
     )

  ); // matches[password] mencocokan password
  $this->form_validation->set_rules('TargetJam','Target Jam','required|numeric',
  array(
             'required'      => 'Form Target Jam tidak boleh kosong %s.',
             'numeric'     => 'Input target jam tidak diperbolehkan dengan huruf, harus angka'

     )

  );
  $this->form_validation->set_rules('NoTelpon','No Telpon','required|numeric',
  array(
    'required'      => 'Form Target Jam tidak boleh kosong %s.',
    'numeric'     => 'No telpon tidak valid'
     )
     );

  if ($this->form_validation->run()==FALSE){
    $this->load->view('manager/registereror');
}

else {
  $data = $this->Register_model->simpandata($dataregister);
  if($data){
   $this->session->set_flashdata('sukses','Data Berhasil Disimpan');

   redirect('Dasboard/registerformfrommanager');

  }else{
    $this->session->set_flashdata('gagal','Data Gagal Disimpan');

    redirect('Dasboard/registerformfrommanager');
  }


}
}
}

//manager

//CEO
public function updateuserfromceo($NIK){

    if(isset($_POST)){

    $dataregister = array(
      'NIK' => $this->input->post('NIK'),
      'IdJabatan' => $this->input->post('Jabatan'),
      'Nama' => $this->input->post('Nama'),
      'JenisKelamin' => $this->input->post('JenisKelamin'),
      'Level' => $this->input->post('Level'),
      'NoTelpon' => $this->input->post('NoTelpon'),
      'Email' => $this->input->post('Email'),
      'TargetJam' =>$this->input->post('TargetJam'),
      'TglLahir' => $this->input->post('TglLahir'),
      'Status' => $this->input->post('Status')


    );




    $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|numeric',
    array(
               'required'      => 'Form NIK tidak boleh kosong %s.',
               'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
               'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
               'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
       )

    );

    $this->form_validation->set_rules('TargetJam','Target Jam','required|numeric',
    array(
      'required'      => 'Form Target Jam tidak boleh kosong %s.',
      'numeric'     => 'Input target jam tidak diperbolehkan dengan huruf,harus angka'

       )

    );
    $this->form_validation->set_rules('NoTelpon','No Telpon','required|numeric',
    array(
      'required'      => 'Form Target Jam tidak boleh kosong %s.',
      'numeric'     => 'No telpon tidak valid'

       )

    );


    $this->form_validation->set_rules('Email','Email','required|valid_email',
    array(
               'required'      => 'Form Email tidak boleh kosong %s.',
               'valid_email'     => 'email anda tidak valid'
       )
    );


    if ($this->form_validation->run()==FALSE){
      $this->load->view('CEO/registererorupdate');
  }

  else {
    $data = $this->Register_model->updateuser($dataregister,$NIK);
    if($data){
     $this->session->set_flashdata('sukses','Data Berhasil Di Update');

     redirect('Dasboard/viewUserCEO');

    }else{
      $this->session->set_flashdata('gagal','Data Gagal  Disimpan');

      redirect('Dasboard/viewUserCEO');
    }



  }

}
}

public function postdatafromceo()
{

  if(isset($_POST)){

  $dataregister = array(
    'NIK' => $this->input->post('NIK'),
    'IdJabatan' => $this->input->post('Jabatan'),
    'Nama' => $this->input->post('Nama'),
    'Username' => $this->input->post('Username'),
    'Password' => base64_encode($this->input->post('Password')),
    'Level' => $this->input->post('Level'),
    'JenisKelamin' => $this->input->post('JenisKelamin'),
    'NoTelpon' => $this->input->post('NoTelpon'),
    'Email' => $this->input->post('Email'),
    'TargetJam' =>$this->input->post('TargetJam'),
    'TglLahir' => $this->input->post('TglLahir'),
    'Status' => $this->input->post('Status')


  );




  $this->form_validation->set_rules('NIK','NIK KTP','required|min_length[16]|max_length[16]|is_unique[user.NIK]|numeric',
  array(
             'required'      => 'Form NIK tidak boleh kosong %s.',
             'min_length'     => 'Minimal 16 digit angka NIK KTP Anda.',
             'max_length'     => 'Maksimal 16 digit angka NIK KTP Anda.',
             'is_unique'     => 'NIK Sudah dipakai',
             'numeric'     => 'NIK tidak valid , NIK harus menggunakan angka'
     )

  );
  $this->form_validation->set_rules('Username','Username','required|min_length[5]|max_length[20]|is_unique[user.Username]',
  array(
             'required'      => 'Form  Username tidak boleh kosong %s.',
             'min_length'     => 'Username Minimal 5 karakter',
             'max_length'     => 'Username Maksimal 20 karakter',
             'is_unique'     => 'Username Sudah dipakai'
     )

  );
  $this->form_validation->set_rules('Email','Email','required|valid_email|is_unique[user.Email]',
  array(
             'required'      => 'Form Email tidak boleh kosong %s.',
             'valid_email'     => 'email anda tidak valid',
             'is_unique'     => 'Email Sudah dipakai'
     )
  );
  $this->form_validation->set_rules('Password','Password','required|min_length[5]'
  ,
  array(
             'required'      => 'Form tidak boleh kosong %s.',
             'min_length'     => 'Password Minimal 5 karakter'
     )

  ); // min_length[5] password tidak boleh kurang dari lima
  $this->form_validation->set_rules('Passwordconf','Retype Password','required|matches[Password]',
  array(
             'required'      => 'Form tidak boleh kosong %s.',
             'matches'     => 'Konfirmasi Password anda tidak cocok'
     )

  ); // matches[password] mencocokan password
  $this->form_validation->set_rules('TargetJam','Target Jam','required|numeric',
  array(
             'required'      => 'Form Target Jam tidak boleh kosong %s.',
             'numeric'     => 'Input target jam tidak diperbolehkan dengan huruf, harus angka'

     )

  );
  $this->form_validation->set_rules('NoTelpon','No Telpon','required|numeric',
  array(
    'required'      => 'Form Target Jam tidak boleh kosong %s.',
    'numeric'     => 'No telpon tidak valid'
     )
     );

  if ($this->form_validation->run()==FALSE){
    $this->load->view('CEO/registereror');
}

else {
  $data = $this->Register_model->simpandata($dataregister);
  if($data){
   $this->session->set_flashdata('sukses','Data Berhasil Disimpan');

   redirect('Dasboard/registerformfromceo');

  }else{
    $this->session->set_flashdata('gagal','Data Gagal Disimpan');

    redirect('Dasboard/registerformfromceo');
  }


}
}
}


// akhir CEO

}


/* End of file Register.php */
/* Location: ./application/controllers/Register.php */
