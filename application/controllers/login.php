<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller LoginController
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Login extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->view('index');
  }

  public function ceklogin(){
    if(isset($_POST['login'])){
      $username = $this->input->post('Username',true);
      $password = base64_encode($this->input->post('Password',true));
      $hasil = $this->Login_model->proseslogin($username,$password);
      if($hasil->Level== 'administrator'){
        $array = array(
          'admin' => 'Administrator',
          'nama' => $hasil->Nama,
          'NIK' =>$hasil->NIK,
          'NoTelpon' => $hasil->NoTelpon,
          'JenisKelamin' => $hasil->JenisKelamin,
          'Email' => $hasil->Email,
          'Jabatan' =>$hasil->Jabatan

        );
        $this->session->set_userdata( $array );

        redirect('Dasboard/admin');

      }elseif($hasil->Level=='karyawan'){
        $array = array(
          'karyawan' => 'karyawan',
          'nama' => $hasil->Nama,
          'NIK' =>$hasil->NIK,
          'NoTelpon' => $hasil->NoTelpon,
          'JenisKelamin' => $hasil->JenisKelamin,
          'Email' => $hasil->Email,
          'Jabatan' =>$hasil->Jabatan
        );
        $this->session->set_userdata( $array );

        redirect('Dasboard/karyawan');
      }elseif($hasil->Level=='CEO'){
        $array = array(
          'CEO' => 'CEO',
          'nama' => $hasil->Nama,
          'NIK' =>$hasil->NIK,
          'NoTelpon' => $hasil->NoTelpon,
          'JenisKelamin' => $hasil->JenisKelamin,
          'Email' => $hasil->Email,
          'Jabatan' =>$hasil->Jabatan
        );
        $this->session->set_userdata( $array );
        redirect('Dasboard/CEO');
      }elseif($hasil->Level=='manager'){
        $array = array(
          'manager' => 'manager',
          'nama' => $hasil->Nama,
          'NIK' =>$hasil->NIK,
          'NoTelpon' => $hasil->NoTelpon,
          'JenisKelamin' => $hasil->JenisKelamin,
          'Email' => $hasil->Email,
          'Jabatan' =>$hasil->Jabatan
        );
        $this->session->set_userdata( $array );
        redirect('Dasboard/manager');
        exit();
      }


    }

  }
  public function logout(){
    $this->session->sess_destroy();
    redirect('login');
  }

}


/* End of file LoginController.php */
/* Location: ./application/controllers/LoginController.php */
