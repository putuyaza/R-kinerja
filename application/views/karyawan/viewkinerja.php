<?php
 if(!$this->session->userdata('NIK')){
     redirect('Eror403');
     exit();
 }


 

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>View Kinerja - Report Kinerja</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Datta Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="admin templates, bootstrap admin templates, bootstrap 4, dashboard, dashboard templets, sass admin templets, html admin templates, responsive, bootstrap admin templates free download,premium bootstrap admin templates, datta able, datta able bootstrap admin template">
    <meta name="author" content="Codedthemes" />
    <!-- <meta http-equiv="refresh" content="10" /> -->

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('assets/images/favicon.ico');?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/animation/css/animate.min.css');?>">
    <!-- vendor css -->

    <link rel="stylesheet" href="<?= base_url('assets/css/style2.css');?>">
    <!-- bootstrap css-->

    <link rel="stylesheet" href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css');?>"/>

    <!-- Smart Wizard css -->
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_arrows.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_circles.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_dots.min.css');?>" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url('assets/plugins/data-tables/css/datatables.min.css');?>">
      <link rel="stylesheet" href="<?= base_url('assets/css/style.css');?>" >
      <!-- Notification css -->
<link href="<?= base_url('assets/plugins/notification/css/notification.min.css');?>" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url('assets/plugins/modal-window-effects/css/md-modal.css');?>">
<!-- Notification Js -->



    <script src="<?= base_url('assets/plugins/jquery/js/jquery.min.js');?>">
</script>


</head>

<body>
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->

    <!-- menu-->
    <!-- [ navigation menu ] start -->
 <nav class="pcoded-navbar theme-horizontal icon-colored">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="index.html" class="b-brand">
                    <div class="b-bg">
                        <i class="feather icon-trending-up"></i>
                    </div>
                    <span class="b-title">Datta Able</span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="#"><span></span></a>
            </div>
            <div class="navbar-content sidenav-horizontal" id="layout-sidenav">
                <ul class="nav pcoded-inner-navbar sidenav-inner">
                    <li class="nav-item pcoded-menu-caption">
                        <label>Navigation</label>
                    </li>
                    <li data-username="dashboard Default Ecommerce CRM Analytics Crypto Project" class="nav-item pcoded-hasmenu active ">
                        <a href="<?= base_url('/');?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>

                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light">
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="#"><span></span></a>
            <a href="<?= base_url('/');?>" class="b-brand">
                   <div class="b-bg">
                       <img class="rlogo" src="<?= base_url('assets/img/Rlogo.png');?>"/>
                   </div>
                   <span class="b-title">Report Kinerja</span>
               </a>
        </div>
        <a class="mobile-menu" id="mobile-header" href="dashboard-ecommerce.html#!">
            <i class="feather icon-more-horizontal"></i>
        </a>
        <div class="collapse navbar-collapse">

            <ul class="navbar-nav ml-auto">
                 <li>
                    <div class="dropdown drp-user">
                        <a href="dashboard-ecommerce.html#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon feather icon-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="<?= base_url('assets/images/user/avatar-1.jpg');?>" class="img-radius" alt="User-Profile-Image">
                                <span><?=
                                $this->session->userdata('nama');
                                 ?></span>
                                <a href="<?= base_url('login/logout');?>" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i>
                                </a>
                            </div>
                            <ul class="pro-body">
                                <li><a href="<?= base_url('Dasboard/edituser/').$this->session->userdata('NIK');  ?>" class="dropdown-item"><i class="feather icon-settings"></i> Edit Profil</a></li>

                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <!-- [ Header ] end -->


<!-- [ chat user list ] start -->
<section class="header-user-list">
    <div class="h-list-header">
        <div class="input-group">
            <input
                type="text"
                id="search-friends"
                class="form-control"
                placeholder="Search Friend . . .">
        </div>
    </div>
    <div class="h-list-body">
        <a href="dashboard-ecommerce.html#!" class="h-close-text">
            <i class="feather icon-chevrons-right"></i>
        </a>
        <div class="main-friend-cont scroll-div">
            <div class="main-friend-list">
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- [ chat user list ] end -->

<!-- [ chat message ] start -->
<section class="header-chat">
    <div class="h-list-header">
        <h6>Josephin Doe</h6>
        <a href="dashboard-ecommerce.html#!" class="h-back-user-list">
            <i class="feather icon-chevron-left"></i>
        </a>
    </div>
    <div class="h-list-body">
        <div class="main-chat-cont scroll-div">
            <div class="main-friend-chat">
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius img-radius m-t-5"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">hello Datta! Will you tell me something</p>
                            <p class="chat-cont">about yourself?</p>
                        </div>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
                <div class="media chat-messages">
                    <div class="media-body chat-menu-reply">
                        <div class="">
                            <p class="chat-cont">Ohh! very nice</p>
                        </div>
                        <p class="chat-time">8:22 a.m.</p>
                    </div>
                </div>
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius img-radius m-t-5"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">can you help me?</p>
                        </div>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="h-list-footer">
        <div class="input-group">
            <input type="file" class="chat-attach" style="display:none">
            <a
                href="dashboard-ecommerce.html#!"
                class="input-group-prepend btn btn-success btn-attach">
                <i class="feather icon-paperclip"></i>
            </a>
            <input
                type="text"
                name="h-chat-text"
                class="form-control h-send-chat"
                placeholder="Write hear . . ">
            <button type="submit" class="input-group-append btn-send btn btn-primary">
                <i class="feather icon-message-circle"></i>
            </button>
        </div>
    </div>
</section>
<!-- [ chat message ] end -->

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->

                <!-- [ breadcrumb ] end -->
                <div class="main-body" id="divrefresh">
                <div class="row">
                   <div class="col">
                   <div class="col borderpage">
                     <div class="row">
                       <div class="col">
                         <h4>View Kinerja</h4>
                         <img src="<?= base_url('assets/img/profil2.png')  ?>" style="width:10%"/>&nbsp;&nbsp;<label>NIK : <?= $this->session->userdata('NIK');; ?>
</label>



                       </div>
                       <div class="col">
                         <h5 style="float:right;" class="hei-100   wid-100 bg-light d-inline-block mr-2 border border-secondary rounded" data-toggle="tooltip" >
                           <p style="font-size:2em;"><?= $sumkinerja; ?></br>Jam</p>

                         </h5>
                       </div>
                       <div class="w-100"></div>


                       <div class="col">
                         <div class="row">
                             <div class="col-sm-12">
                             <div class="card">
                             <div class="card-header">
                             <h5>Kinerja <?php echo $this->session->userdata('nama'); ?></h5>
                             <?php
                                 if($this->session->flashdata('sukses')){
                                     echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('sukses')."</div>";

                                 }elseif($this->session->flashdata('gagal')){
                                     echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('gagal')."</div>";
                                 }

                                 ?>
                             </div>
                             <div class="card-block">
                             <div class="table-responsive">

                                 <table id="fixed-header" class="display table nowrap table-striped table-hover" style="width:100%">

                         <thead>
                             <tr>
                             <th>Tanggal</th>
                             <th>Description</th>
                             <th>Estimasi Per Jam</th>
                             <th>Action</th>

                             </tr>
                             </thead>
                             <tbody>
                               <?php foreach ($hasil->result() as $result) : ?>

                             <tr>
                               <input type="hidden" name="" value="<?= $result->NIK; ?>">
                               <td><?= $result->Tanggal; ?></td>
                             <td><?= $result->Jobdesk; ?></td>
                             <td id="jamkerja"><?= $result->EstimasiKerja;  ?></td>


                             <td>
                               <a link href="#" class="md-trigger" data-modal="datauser<?=  $result->KodeJobdesk;?>"  data-target="#datauser<?=  $result->KodeJobdesk;?>"><i class="feather icon-edit-2"></i></a>
                               | <a link href="<?= base_url('Jobdesk/deletejobperkaryawan/'. $result->KodeJobdesk);?>"><i class="feather icon-trash-2" onclick="return confirm('Apakah Anda Ingin Menghapus Jobdesk ini');"></i></a></td>

                             </tr>
                               <?php endforeach; ?>
                             </tbody>
                             <tfoot>
                             <!-- <tr>
                             <th>Name</th>
                             <th>Position</th>
                             <th>Office</th>
                             <th>Age</th>
                             <th>Start date</th>
                             <th>Salary</th>
                             </tr> -->
                             </tfoot>
                             </table>
                             </div>
                             </div>
                             </div>
                             </div>
                             </div>
                       </div>

                       <!-- modal start -->


                       <?php
                    $no =1;
                      foreach ($hasil->result() as $result) :
                      ?>
                       <div class="md-modal md-effect-3" style="border:1px solid #b2bec3;"  id="datauser<?= $result->KodeJobdesk;?>">

                       <div class="md-content " >

                       <h3 class="theme-bg2">Edit data  <a link href="#" class="md-close" style="float:right;"><i class="feather icon-x"></i></a></h3>

                            <div id="datauser<?= $result->KodeJobdesk;?>">

                       <form name="edituser" method="POST" action="<?= base_url('Jobdesk/updatejobperkaryawan/'.$result->KodeJobdesk)?>">
                         <div class="card-block table-border-style ">
                     <div class="table-responsive">
                     <table class="table ">

                     <!-- <thead>
                     <tr>
                     <th>#</th>
                     <th>First Name</th>
                     <th>Last Name</th>
                     <th>Username</th>
                     </tr>
                     </thead> -->
                     <tbody>

                     <tr>
                     <td>Jobdesk</td>
                     <td>:</td>
                     <td><input class="form-control" name="Jobdesk"  type="text" value="<?=$result->Jobdesk;?>"/></td>
                     <input type="hidden" name="TglJobdesk" value="<?= $result->Tanggal; ?>">
                     <input type="hidden" name="NIK" value="<?= $result->NIK; ?>">
                     </tr>
                     <tr>


                     <td>Estimasi</td>
                     <td>:</td>
                     <td><input class="form-control" name="Jam" type="text" value="<?=$result->EstimasiKerja;?>"/></td>
                     </tr>

                     </tbody>
                     </table>

                     </div>
                     </div>


                       <button type="submit" name="update" class="btn btn-primary" data-toggle="tooltip">Update</button>
                       </form>
                       <!-- <button class="btn btn-primary md-close">Close me!</button> -->

                       </div>

                       </div>

                       </div>
                         <?php endforeach; ?>
                       <!-- modal stop -->


                     </div>



                   </div>

                   </div>
                  <div class="col-sm-3">
                    <?php include('panelkanan.php'); ?>
                  </div>

                </div>


                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</div>
</div>
</div>
</div>


<?php
include('footer.php');
?>
