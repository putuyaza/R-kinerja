<?php
 if(!$this->session->userdata('NIK')){
     redirect('Eror403');

 }






?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title> Edit data User - R-Kinerja</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content="CodedThemes" />

    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url('assets/img/faviconR.ico');?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css');?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/animation/css/animate.min.css');?>">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/css/style2.css');?>">


</head>

<body>
    <div class="auth-wrapper aut-bg-img-side cotainer-fiuid align-items-stretch ">
        <div class="row align-items-center w-100 align-items-stretch bg-white " >
            <div class="d-none d-lg-flex col-lg-8 aut-bg-img align-items-center d-flex justify-content-center">
                <div class="col-md-8 fixed-top" style="margin-top:10%;">
                  <img class="logoharmoni" src="<?= base_url('assets/img/logoharmonipermata.png');?>"/>
                  <h1 align="center" class="text-white mb-5">Report Kinerja</h1>
                  <p align="center" class="text-white" >CV.Harmoni Permata</p>
                </div>
            </div>
              <?php foreach ($hasil->result() as $result) : ?>
            <div class="col-lg-4 align-items-stret h-100 align-items-center d-flex justify-content-center">
                <div class=" auth-content text-center">
                    <div class="mb-4">
                        <i class="feather icon-user-plus auth-icon"></i>
                    </div>
                    <h3 class="mb-4">Update Data</h3>
                    <?php
                        if($this->session->flashdata('sukses')){
                            echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('sukses')."</div>";

                        }elseif($this->session->flashdata('gagal')){
                            echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('gagal')."</div>";
                        }

                        ?>






                       <?php
                        echo form_open_multipart('Register/updateperuser/'.$result->NIK);
                       ?>
                    <div class="input-group mb-3">
                        <input type="hidden" name="NIK" class="form-control" placeholder="NIK" value="<?= $result->NIK;?>">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="Nama" value="<?= $result->Nama;  ?>" placeholder="Nama lengkap" required>
                    </div>
                    <div class="input-group mb-4">
                        <input type="text"  name="Username" class="form-control" value="<?= $result->Username;  ?>" placeholder="Username" required>
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" name="Password" class="form-control" value="<?= base64_decode($result->Password);?>" placeholder="Password" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="Passwordconf" value="<?= base64_decode($result->Password); ?>" class="form-control" placeholder="Konfirmasi Password" required>
                    </div>
                    <div class="input-group mb-3">

                      <select class="form-control" name="Level">
                          <option value="<?= $result->Level;  ?>" selected><?= $result->Level;  ?></option>
                          <option value="administrator">administrator</option>
                          <option value="karyawan">karyawan</option>
                          <option value="manager">manager</option>
                          <option value="CEO">CEO</option>
                      </select>
                    </div>
                    <div class="input-group mb-3">
                      <select class="form-control" name="JenisKelamin">
                        <option value="<?= $result->JenisKelamin;  ?>" selected><?= $result->JenisKelamin;  ?></option>
                          <option value="L">Laki-Laki</option>
                          <option value="P">Perempuan</option>
                        </select>

                    </div>
                    <div class="input-group mb-3">
                        <input  type="text" name="NoTelpon" value="<?= $result->NoTelpon;  ?>" class="form-control" placeholder="No Telpon" required>
                    </div>
                    <div class="input-group mb-3">
                        <input  type="date" name="TglLahir" value="<?= $result->TglLahir;  ?>" class="form-control" required/>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" name="Email" value="<?= $result->Email;  ?>" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="input-group mb-3">
                        <input  type="text" name="TargetJam" value="<?= $result->TargetJam;  ?>" class="form-control" placeholder="Target jam kerja" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="Status" value="<?= $result->Status;  ?>" class="form-control" placeholder="Status" required>
                    </div>
                    <div class="input-group mb-3">
                      <select class="form-control" name="Jabatan">
                          <option value="<?= $result->IdJabatan ?>" selected><?= $result->IdJabatan ?> <?= $result->Jabatan ?></option>
                          <option value="1">1 Admin</option>
                          <option value="2">2 Marketing</option>
                          <option value="3">3 Manager</option>
                          <option value="4">4 CEO</option>
                          <option value="5">5 Public Relationship</option>
                          <option value="6">6 Programer</option>
                          <option value="7">7 Designer</option>
                          <option value="8">8 Tester dan Implementator</option>

                      </select>
                    </div>


                  <p class="mb-0 text-muted"><a href="<?= base_url('');?>"> Kembali Ke Dashboard</a></p>

                    <button type="submit" id="klik" class="btn btn-primary shadow-2 mb-4" >Update</button>

                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
          <?php endforeach; ?>
    </div>


    <!-- Required Js -->
    <script src="<?php echo base_url('assets/js/vendor-all.min.js');?>"></script><script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- <script src="<?php echo base_url('assets/js/pcoded.min.js');?>"></script> -->
    <script src="<?= base_url('assets/plugins/jquery/js/jquery.min.js');?>">
</script>




  </script>

</body>
</html>
