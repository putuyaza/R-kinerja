<?php
 if(!$this->session->userdata('manager')){
     redirect('Eror403');
     exit();
 }

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Print Laporan Gaji</title>
  </head>

  <body>

              <div style="margin-left:37%;">
              <table>

                <tr>
                  <td>
                    <h3 style="text-align:center;font-size:2rem;">
                      <br>Laporan Gaji</br>
                      <br>CV. Harmoni Permata </br>
                      <br>
                      <?php
                      $datestring = '%Y ';
                      // $tgl=Date('d-m-y');
                      echo mdate($datestring);
                      ?></br>
                    </h3>

                  </td>
                  <td>&nbsp;</td>
                </tr>
              </table>

    </div>
    <hr>

    <br></br>

<table border="1px" style="width:100%;">
    <tr>
    <th>NO</th>
    <th>Nama</th>
    <th>Invoice</th>
    <th>Target</th>
    <th>Total Jam</th>
    <th>Jabatan</th>
    <th>Gaji Pokok</th>
    <th>Bonus</th>
    <th>Tunjangan</th>
    <th>Total Gaji</th>
    <th>Status</th>
    </tr>
        <?php
      $no =1;
      foreach ($hasilgaji->result() as $result){
        ?>

    <tr>
    <td><?= $no++; ?></td>
    <td><?= $result->Nama; ?></td>
    <td>GHP-<?= $result->InvoiceGaji; ?></td>
    <td><?= $result->TargetJam; ?></td>
    <td><?= $controller->sumdata($result->NIK);?></td>
    <td><?= $result->Jabatan; ?></td>
    <td>
      <?php
      if($controller->sumdata($result->NIK) >= $result->TargetJam ){
        echo "Rp &nbsp;".rupiah($gaji1 = $result->GajiPokok);
      }else{
         $gaji1 = ($result->GajiPokok/$result->TargetJam)*$controller->sumdata($result->NIK);
         echo "Rp &nbsp;".rupiah($gaji1);
      }
      ?>
    </td>
    <td><?= "Rp &nbsp;".rupiah($result->Bonus); ?> </td>
    <td>

      <?php if($result->Tunjangan > 0){
        echo "Rp &nbsp;".rupiah($result->Tunjangan);
      } else{
        echo "0";
      }

      ?>
    </td>
    <td>
      <?php
      $gajipokok = $gaji1;
      $tunjangan = $result->Tunjangan;
      $bonus = $result->Bonus;
      $gaji= $result->Gaji;
      if ($result->Gaji>0) {
        $totalsemua=$gaji + $tunjangan + $bonus;
        echo "Rp &nbsp;".rupiah($totalsemua);
      }else{
        $totalsemua=$gajipokok + $tunjangan + $bonus;
        echo "Rp &nbsp;".rupiah($totalsemua);
      }
      // echo "gajipoko &nbsp;".$gajipokok."</br>";
      //   echo "Tunjangan &nbsp;".$tunjangan."</br>";
      //     echo "Bonus &nbsp;".$bonus."</br>";


       ?>
   </td>
   <td>
     <?php
     $status=$result->Status;
    if ($status>0) {
      if($controller->sumdata($result->NIK) >= $result->TargetJam ){
        echo "<p>Target terpenuhi</p>";
      }else{
        echo "<p style='color:red'>Target tidak terpenuhi</p>";
      }
       echo "Sudah dibayar";
     }else{
       if($controller->sumdata($result->NIK) >= $result->TargetJam ){
         echo "Target terpenuhi";
       }else{
         echo "<p style='color:red'>Target tidak terpenuhi</p>";
       }
       echo "<p style='color:red'>Belum dibayar</p>";
     }
      ?>

   </td>
    </tr>

  <?php } ?>

    </table>
    <table style="float:right;">
      <tr style="text-align:left;">
        <td colspan="3">Mengetahui,</td>
      </tr>
      <tr>
        <td>General Manager</td>
        <td></td>
        <td>Evan Manager</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr style="text-align:center;">
        <td>CEO</td>
        <td></td>
        <td>Hendika Permana</td>
        <td>&nbsp;</td>
      </tr>

    </table>




  </body>
</html>
