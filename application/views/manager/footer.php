
    <div class="container">
    <p align="center">&copy;2019|Report Kinerja|All rights reserved</p>
    </div>
    <!-- Required Js -->
    <script src="<?= base_url('assets/js/vendor-all.min.js');?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
      <script src="<?= base_url('assets/js/pcoded.min.js');?>"></script>
    <!-- <script src="<?= base_url('assets/js/menu-setting.min.js');?>"></script> -->

    <!-- amchart js -->
    <script src="<?= base_url('assets/plugins/amchart/js/amcharts.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/gauge.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/serial.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/light.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/pie.min.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/ammap.min.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/usaLow.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/radar.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/worldLow.js');?>"></script>


    <!-- dashboard-custom js -->
    <script src="<?= base_url('assets/js/pages/dashboard-ecommerce.js');?>"></script>

<script src="<?= base_url('assets/js/horizontal-menu.js');?>">
</script>
<script src="<?= base_url('assets/plugins/notification/js/bootstrap-growl.min.js');?>"></script>

<script src="<?= base_url('assets/plugins/modal-window-effects/js/classie.js');?>"></script>
<script src="<?= base_url('assets/plugins/modal-window-effects/js/modalEffects.js');?>"></script>
<script src="<?= base_url('assets/plugins/data-tables/js/datatables.min.js');?>"></script>

<!-- jquery js-->

<script type="text/javascript">

      //Collapse menu
      (function() {
          if ($('#layout-sidenav').hasClass('sidenav-horizontal') || window.layoutHelpers.isSmallScreen()) {
              return;
          }
          try {
              window.layoutHelpers.setCollapsed(
                  localStorage.getItem('layoutCollapsed') === 'true',
                  false
              );
          } catch (e) {}
      })();
      $(function() {
          // Initialize sidenav
          $('#layout-sidenav').each(function() {
              new SideNav(this, {
                  orientation: $(this).hasClass('sidenav-horizontal') ? 'horizontal' : 'vertical'
              });
          });

          // Initialize sidenav togglers
          $('body').on('click', '.layout-sidenav-toggle', function(e) {
              e.preventDefault();
              window.layoutHelpers.toggleCollapsed();
              if (!window.layoutHelpers.isSmallScreen()) {
                  try {
                      localStorage.setItem('layoutCollapsed', String(window.layoutHelpers.isCollapsed()));
                  } catch (e) {}
              }
          });
      });
      $(document).ready(function() {
          $("#pcoded").pcodedmenu({
              themelayout: 'horizontal',
              MenuTrigger: 'hover',
              SubMenuTrigger: 'hover',
          });
      });


</script>


<script src="<?= base_url('assets/plugins/data-tables/js/datatables.min.js');?>"></script>
<script src="<?= base_url('assets/js/pages/tbl-datatable-custom.js');?>"></script>


</body>
</html>
