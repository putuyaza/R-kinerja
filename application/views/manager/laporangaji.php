<?php
include('header.php');
 ?>



 <!-- [ chat user list ] start -->
 <section class="header-user-list">
     <div class="h-list-header">
         <div class="input-group">
             <input
                 type="text"
                 id="search-friends"
                 class="form-control"
                 placeholder="Search Friend . . .">
         </div>
     </div>
     <div class="h-list-body">
         <a href="dashboard-ecommerce.html#!" class="h-close-text">
             <i class="feather icon-chevrons-right"></i>
         </a>
         <div class="main-friend-cont scroll-div">
             <div class="main-friend-list">
                 <div
                     class="media userlist-box"
                     data-id="1"
                     data-status="online"
                     data-username="Josephin Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image ">
                         <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                             </small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="2"
                     data-status="online"
                     data-username="Lary Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="3"
                     data-status="online"
                     data-username="Alice">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="4"
                     data-status="offline"
                     data-username="Alia">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="5"
                     data-status="offline"
                     data-username="Suzen">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="1"
                     data-status="online"
                     data-username="Josephin Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image ">
                         <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                             </small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="2"
                     data-status="online"
                     data-username="Lary Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="3"
                     data-status="online"
                     data-username="Alice">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="4"
                     data-status="offline"
                     data-username="Alia">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="5"
                     data-status="offline"
                     data-username="Suzen">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="1"
                     data-status="online"
                     data-username="Josephin Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image ">
                         <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                             </small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="2"
                     data-status="online"
                     data-username="Lary Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="3"
                     data-status="online"
                     data-username="Alice">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="4"
                     data-status="offline"
                     data-username="Alia">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="5"
                     data-status="offline"
                     data-username="Suzen">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="1"
                     data-status="online"
                     data-username="Josephin Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image ">
                         <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                             </small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="2"
                     data-status="online"
                     data-username="Lary Doe">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="3"
                     data-status="online"
                     data-username="Alice">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="4"
                     data-status="offline"
                     data-username="Alia">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                         alt="Generic placeholder image">
                         <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                         <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                         </h6>
                     </div>
                 </div>
                 <div
                     class="media userlist-box"
                     data-id="5"
                     data-status="offline"
                     data-username="Suzen">
                     <a class="media-left" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius"
                         src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body">
                         <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                         </h6>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!-- [ chat user list ] end -->

 <!-- [ chat message ] start -->
 <section class="header-chat">
     <div class="h-list-header">
         <h6>Josephin Doe</h6>
         <a href="dashboard-ecommerce.html#!" class="h-back-user-list">
             <i class="feather icon-chevron-left"></i>
         </a>
     </div>
     <div class="h-list-body">
         <div class="main-chat-cont scroll-div">
             <div class="main-friend-chat">
                 <div class="media chat-messages">
                     <a class="media-left photo-table" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius img-radius m-t-5"
                         src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body chat-menu-content">
                         <div class="">
                             <p class="chat-cont">hello Datta! Will you tell me something</p>
                             <p class="chat-cont">about yourself?</p>
                         </div>
                         <p class="chat-time">8:20 a.m.</p>
                     </div>
                 </div>
                 <div class="media chat-messages">
                     <div class="media-body chat-menu-reply">
                         <div class="">
                             <p class="chat-cont">Ohh! very nice</p>
                         </div>
                         <p class="chat-time">8:22 a.m.</p>
                     </div>
                 </div>
                 <div class="media chat-messages">
                     <a class="media-left photo-table" href="dashboard-ecommerce.html#!"><img
                         class="media-object img-radius img-radius m-t-5"
                         src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                         alt="Generic placeholder image"></a>
                     <div class="media-body chat-menu-content">
                         <div class="">
                             <p class="chat-cont">can you help me?</p>
                         </div>
                         <p class="chat-time">8:20 a.m.</p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="h-list-footer">
         <div class="input-group">
             <input type="file" class="chat-attach" style="display:none">
             <a
                 href="dashboard-ecommerce.html#!"
                 class="input-group-prepend btn btn-success btn-attach">
                 <i class="feather icon-paperclip"></i>
             </a>
             <input
                 type="text"
                 name="h-chat-text"
                 class="form-control h-send-chat"
                 placeholder="Write hear . . ">
             <button type="submit" class="input-group-append btn-send btn btn-primary">
                 <i class="feather icon-message-circle"></i>
             </button>
         </div>
     </div>
 </section>
 <!-- [ chat message ] end -->

 <!-- [ Main Content ] start -->
 <div class="pcoded-main-container">
     <div class="pcoded-wrapper">
         <div class="pcoded-content">
             <div class="pcoded-inner-content">
                 <!-- [ breadcrumb ] start -->

                 <!-- [ breadcrumb ] end -->
                 <div class="main-body" id="divrefresh">
                 <div class="row">
                    <div class="col">
                    <div class="col borderpage">
                      <div class="row">
                          <div class="col-sm-12">
                          <div class="card">
                          <div class="card-header">
                          <h5>Laporan Gaji</h5>
                          <?php
                              if($this->session->flashdata('sukses')){
                                  echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('sukses')."</div>";

                              }elseif($this->session->flashdata('gagal')){
                                  echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('gagal')."</div>";
                              }

                              ?>
                          </div>
                          <div class="card-block">
                          <div class="table-responsive">
                              <table id="zero-configuration" class="display table nowrap table-striped table-hover" style="width:100%">

                      <thead>
                          <tr>
                          <th>NO</th>
                          <th>Nama</th>
                          <th>Invoice</th>
                          <th>Target</th>
                          <th>Total Jam</th>
                          <th>Jabatan</th>
                          <th>Gaji Pokok</th>
                          <th>Bonus</th>
                          <th>Tunjangan</th>
                          <th>Total Gaji</th>
                          <th>Verifikasi</th>
                          </tr>
                          </thead>

                          <tbody>

                            <?php
                            $no =1;
                            foreach ($hasilgaji->result() as $result){
                              ?>

                          <tr>
                          <td><?= $no++; ?></td>
                          <td><?= $result->Nama; ?></td>
                          <td>GHP-<?= $result->InvoiceGaji; ?></td>
                          <td><?= $result->TargetJam; ?></td>
                          <td><?= $controller->sumdata($result->NIK);?></td>
                          <td><?= $result->Jabatan; ?></td>
                          <td>
                            <?php
                            if($controller->sumdata($result->NIK) >= $result->TargetJam ){
                              echo "Rp &nbsp;".rupiah($gaji1 = $result->GajiPokok);
                            }else{
                               $gaji1 = ($result->GajiPokok/$result->TargetJam)*$controller->sumdata($result->NIK);
                               echo "Rp &nbsp;".rupiah($gaji1);
                            }
                            ?>
                          </td>
                          <td><?= "Rp &nbsp;".rupiah($result->Bonus); ?> &nbsp;<i class="feather icon-edit-1"></td>
                          <td>

                            <?php if($result->Tunjangan > 0){
                              echo "Rp &nbsp;".rupiah($result->Tunjangan);
                            } else{
                              echo "0";
                            }

                            ?>
                          </td>
                          <td>
                            <?php
                            $gajipokok = $gaji1;
                            $tunjangan = $result->Tunjangan;
                            $bonus = $result->Bonus;
                            $gaji= $result->Gaji;
                            if ($result->Gaji>0) {
                              $totalsemua=$gaji + $tunjangan + $bonus;
                                echo "Rp &nbsp;".rupiah($totalsemua);
                            }else{
                              $totalsemua=$gajipokok + $tunjangan + $bonus;
                                echo "Rp &nbsp;".rupiah($totalsemua);

                            }
                            // echo "gajipoko &nbsp;".$gajipokok."</br>";
                            //   echo "Tunjangan &nbsp;".$tunjangan."</br>";
                            //     echo "Bonus &nbsp;".$bonus."</br>";


                             ?>
                         </td>
                         <td>
                           <?php
                           $status=$result->Status;
                          if ($status>0) {
                             echo "Gaji Sudah Di Bayar";
                           }else{
                             echo "<a href='#' class='md-trigger' data-modal='datauser$result->NIK'  data-target='#datauser$result->NIK'><i class='feather icon-check'></i> bayar</a>";
                           }
                            ?>

                         </td>
                          </tr>

                        <?php } ?>
                          </tbody>
                          <tfoot>
                          <!-- <tr>
                          <th>Name</th>
                          <th>Position</th>
                          <th>Office</th>
                          <th>Age</th>
                          <th>Start date</th>
                          <th>Salary</th>
                          </tr> -->
                          </tfoot>
                          </table>
                            <a href="<?= base_url('Dasboard/exportlaporangajifrommanager') ?>" class="btn btn-primary"><i class="feather icon-printer"></i>Print</a>
                          <!-- modal start -->

                          <?php foreach ($hasilgaji->result() as $result) : ?>

                          <div class="md-modal md-effect-3" style="border:1px solid #b2bec3;"  id="datauser<?= $result->NIK;?>">

                          <div class="md-content" >

                          <h3 class="theme-bg2">Bayar Gaji <?= $result->Nama; ?>  <a link href="#" class="md-close" style="float:right;"><i class="feather icon-x"></i></a></h3>

                               <div id="datauser<?= $result->NIK;?>">

                          <form name="edituser" method="POST" action="<?= base_url('Jobdesk/tambahkinerjafrommanager/'.$result->NIK)  ?>">
                            <div class="card-block table-border-style ">
                        <div class="table-responsive">
                        <table class="table ">

                        <!-- <thead>
                        <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                        </tr>
                        </thead> -->
                        <tbody>

                        <tr>
                        <td>Bonus Gaji</td>
                        <td>:</td>
                        <td><input class="form-control" name="Bonus"  type="text" placeholder="Tambah Bonus"/>
                          <input type="hidden" name="NIK" value="<?= $result->NIK; ?>" >
                          <?php
                          if($controller->sumdata($result->NIK) >= $result->TargetJam ){
                          $gaji2 = $result->GajiPokok;
                          }else{
                             $gaji2 = ($result->GajiPokok/$result->TargetJam)*$controller->sumdata($result->NIK);

                          }
                          $gajipokok = $gaji2;
                          $tunjangan = $result->Tunjangan;
                          $bonus = $result->Bonus;
                          $totalsemuagaji=$gajipokok + $tunjangan + $bonus;

                           ?>
                          <input type="hidden" name="Gaji" value="<?php echo $totalsemuagaji;?>">

                        </td>
                        </tr>
                        </tbody>
                        </table>

                        </div>
                        </div>

                          <button type="submit" class="btn btn-primary" data-toggle="tooltip">Bayar Gaji</button>
                          <p align="center">Form bonus gaji dapat dikosongkan</p>
                          </form>
                          <!-- <button class="btn btn-primary md-close">Close me!</button> -->

                          </div>

                          </div>

                          </div>
                        <?php endforeach; ?>
                          <!-- modal stop -->
                          </div>
                          </div>
                          </div>
                          </div>
                          </div>


                    </div>

                    </div>



                 </div>


                 </div>

             </div>
             <!-- [ Main Content ] end -->

         </div>
     </div>
 </div>
 </div>
 </div>
 </div>



 <script>

 var myCustomScrollbar = document.querySelector('.my-custom-scrollbar');
var ps = new PerfectScrollbar(myCustomScrollbar);

var scrollbarY = myCustomScrollbar.querySelector('.ps.ps--active-y>.ps__scrollbar-y-rail');

myCustomScrollbar.onscroll = function() {
scrollbarY.style.cssText = `top: ${this.scrollTop}px!important; height: 400px; right: ${-this.scrollLeft}px`;
}
 </script>


 <?php
include('footer.php');
  ?>
