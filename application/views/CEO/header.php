<?php
 if(!$this->session->userdata('CEO')){
     redirect('Eror403');
     exit();
 }

 $sesi=$this->session->userdata('CEO');


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dasboard Manager - Report Kinerja</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Datta Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="admin templates, bootstrap admin templates, bootstrap 4, dashboard, dashboard templets, sass admin templets, html admin templates, responsive, bootstrap admin templates free download,premium bootstrap admin templates, datta able, datta able bootstrap admin template">
    <meta name="author" content="Codedthemes" />
    <!-- <meta http-equiv="refresh" content="10" /> -->

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('assets/images/favicon.ico');?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/animation/css/animate.min.css');?>">
    <!-- vendor css -->

    <link rel="stylesheet" href="<?= base_url('assets/css/style2.css');?>">
    <!-- bootstrap css-->

    <link rel="stylesheet" href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css');?>"/>

    <!-- Smart Wizard css -->
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_arrows.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_circles.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_dots.min.css');?>" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url('assets/plugins/data-tables/css/datatables.min.css');?>">
      <link rel="stylesheet" href="<?= base_url('assets/css/style.css');?>" >
      <!-- Notification css -->
<link href="<?= base_url('assets/plugins/notification/css/notification.min.css');?>" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url('assets/plugins/modal-window-effects/css/md-modal.css');?>">
<!-- Notification Js -->



    <script src="<?= base_url('assets/plugins/jquery/js/jquery.min.js');?>">
</script>

</head>

<body>
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->

    <!-- menu-->
    <!-- [ navigation menu ] start -->
 <nav class="pcoded-navbar theme-horizontal icon-colored">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="index.html" class="b-brand">
                    <div class="b-bg">
                        <i class="feather icon-trending-up"></i>
                    </div>
                    <span class="b-title">Datta Able</span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="#"><span></span></a>
            </div>
            <div class="navbar-content sidenav-horizontal" id="layout-sidenav">
                <ul class="nav pcoded-inner-navbar sidenav-inner">
                    <li class="nav-item pcoded-menu-caption">
                        <label>Navigation</label>
                    </li>
                    <li data-username="dashboard Default Ecommerce CRM Analytics Crypto Project" class="nav-item pcoded-hasmenu active ">
                        <a href="<?= base_url('/');?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>

                    </li>
                    <li data-username="Vertical Horizontal Box Layout RTL fixed static collapse menu color icon dark" class="nav-item pcoded-hasmenu">
                        <a href="<?= base_url('Dasboard/laporankinerjafromceo') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Laporan Kinerja</span></a>

                    </li>
                    <li data-username="widget Statistic Data Table User card Chart" class="nav-item pcoded-hasmenu">
                        <a href="<?= base_url('Dasboard/laporangajifromceo') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-layers"></i></span><span class="pcoded-mtext">Laporan Gaji</span></a>

                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light">
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="#"><span></span></a>
            <a href="<?= base_url('/');?>" class="b-brand">
                   <div class="b-bg">
                       <img class="rlogo" src="<?= base_url('assets/img/Rlogo.png');?>"/>
                   </div>
                   <span class="b-title">Report Kinerja</span>
               </a>
        </div>
        <a class="mobile-menu" id="mobile-header" href="dashboard-ecommerce.html#!">
            <i class="feather icon-more-horizontal"></i>
        </a>
        <div class="collapse navbar-collapse">

            <ul class="navbar-nav ml-auto">
                 <li>
                    <div class="dropdown drp-user">
                        <a href="dashboard-ecommerce.html#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon feather icon-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="<?= base_url('assets/images/user/avatar-1.jpg');?>" class="img-radius" alt="User-Profile-Image">
                                <span><?=
                                $this->session->userdata('nama');
                                 ?></span>
                                <a href="<?= base_url('login/logout');?>" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i>
                                </a>
                            </div>
                            <ul class="pro-body">
                                <li><a href="<?= base_url('Dasboard/edituser/'.$this->session->userdata('NIK'))?>" class="dropdown-item"><i class="feather icon-settings"></i> Edit Profil</a></li>

                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <!-- [ Header ] end -->
