<?php
 if(!$this->session->userdata('CEO')){
     redirect('Eror403');
     exit();
 }

  $sesi=$this->session->userdata('CEO');



?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Slip Gaji</title>

  <body>
    <div style="width:100%;Position:relative">

        <?php

        foreach ($hasilgaji->result() as $result){

          ?>

          <table border="1" style="margin-bottom:1%;margin:auto;">

            <tr>
              <td colspan="2" style="text-align:center;">Slip Gaji</td>
            </tr>
            <tr>
              <td width="200px">CV. harmoni Permata</td>
              <td rowspan="2">Harmoni Permata</td>
            </tr>
            <tr>
              <td>Tanggal : <?php
              $datestring = '%d-%m-%Y ';
              // $tgl=Date('d-m-y');
              echo mdate($datestring);
              ?></br> </td>

            </tr>
            <tr>
              <td>NIK</td>
              <td><?= $result->NIK; ?></td>
            </tr>
            <tr>
              <td>Nama</td>
              <td><?= $result->Nama; ?></td>
            </tr>
            <tr>
              <td>Target Kinerja</td>
              <td><?= $result->TargetJam; ?></td>
            </tr>
            <tr>
              <td>Estimasi Kinerja</td>
              <td><?= $controller->sumdata($result->NIK);?></td>
            </tr>
            <tr>
              <td>Status</td>
              <td><?php
              $status=$result->Status;
             if ($status>0) {
               if($controller->sumdata($result->NIK) >= $result->TargetJam ){
                 echo "Target terpenuhi";
               }else{
                 echo "<p style='color:red'>Target tidak terpenuhi</p>";
               }

              }else{
                if($controller->sumdata($result->NIK) >= $result->TargetJam ){
                  echo "Target terpenuhi";
                }else{
                  echo "<p style='color:red'>Target tidak terpenuhi</p>";
                }

              }
               ?>
</td>
            </tr>
            <tr>
              <td>Gaji pokok</td>
              <td>  <?php
                if($controller->sumdata($result->NIK) >= $result->TargetJam ){
                  if($result->Status==1){
                  echo "Rp &nbsp;".rupiah($gaji1 = $result->GajiPokok);
                }elseif($result->Status==0){
                  echo "<i style='color:red;'>Belum Dibayar</i>";
                }
                }else{
                   $gaji1 = ($result->GajiPokok/$result->TargetJam)*$controller->sumdata($result->NIK);
                   if($result->Status==1){
                   echo "Rp &nbsp;".rupiah($gaji1);
                 }elseif($result->Status==0){
                   echo "<i style='color:red;'>Belum Dibayar</i>";
                 }

                }
                ?></td>
            </tr>
            <tr>
              <td>Bonus</td>
              <td><?= "Rp &nbsp;".rupiah($result->Bonus); ?> </td>
            </tr>
            <tr>
              <td>Tunjangan</td>

              <td><?php
                  if($result->Tunjangan > 0){
                  echo "Rp &nbsp;".rupiah($result->Tunjangan);
                } else{
                  echo "0";
                }

                ?> </td>
            </tr>
            <tr>
              <td>Gaji Diterima</td>
              <td><?php
              $gajipokok = $result->GajiPokok;
              $tunjangan = $result->Tunjangan;
              $bonus = $result->Bonus;
              $gaji= $result->Gaji;
              if ($result->Gaji>0) {
                if($result->Status==1){
              $totalsemua=$gaji + $tunjangan + $bonus;
              echo "Rp &nbsp;".rupiah($totalsemua);
              }elseif($result->Status==0){
                echo "<i style='color:red;'>Belum Dibayar</i>";
              }
              }else{

                if($result->Status==1){
                $totalsemua=$gajipokok + $tunjangan + $bonus;
                echo "Rp &nbsp;".rupiah($totalsemua);
                }elseif($result->Status==0){
                echo "<i style='color:red;'>Belum Dibayar</i>";
                }

              }?></td>
            </tr>
          </table>

      <?php
     } ?>

</div>



</html>
