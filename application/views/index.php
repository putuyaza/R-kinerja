<?php
if($this->session->userdata('admin')){

    redirect('Dasboard/admin');
}elseif($this->session->userdata('karyawan')){
    redirect('Dasboard/karyawan');
}elseif($this->session->userdata('CEO')){
    redirect('Dasboard/CEO');
}elseif($this->session->userdata('manager')){
    redirect('Dasboard/manager');
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login R-Kinerja</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content="CodedThemes" />

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('assets/img/faviconR.ico');?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?= base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css');?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/animation/css/animate.min.css')?>">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css');?>"/>

    <link rel="stylesheet" href="<?= base_url('assets/css/style2.css');?>"/>

</head>

<body>
    <div class="auth-wrapper aut-bg-img-side cotainer-fiuid align-items-stretch">
        <div class="row align-items-center w-100 align-items-stretch bg-white">
            <div class="d-none d-lg-flex col-lg-8 aut-bg-img align-items-center d-flex justify-content-center">
                <div class="col-md-8">
                    <img class="logoharmoni" src="<?= base_url('assets/img/logoharmonipermata.png');?>"/>
                    <h1 align="center" class="text-white mb-5">Report Kinerja</h1>
                    <p align="center" class="text-white" >CV.Harmoni Permata</p>

                </div>
            </div>
            <div class="col-lg-4 align-items-stret h-100 align-items-center d-flex justify-content-center">
                <div class=" auth-content text-center">
                    <div class="mb-4">
                        <i class="feather icon-unlock auth-icon"></i>
                    </div>
                    <h3 class="mb-4">Login</h3>
                    <?php
                    if($this->session->flashdata('msg')){
                        echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('msg')."</div>";

                    }
                    if($this->session->flashdata('sukses')){
                        echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('sukses')."</div>";

                    }

                    ?>
                    <?php echo form_open('login/ceklogin') ?>

                    <div class="input-group mb-3">
                        <input type="text" name="Username" class="form-control" placeholder="Username" required>
                    </div>
                    <div class="input-group mb-4">
                        <input type="password" name="Password" class="form-control" placeholder="password" required>
                    </div>

                    <input type="submit" name="login" value="Login" class="btn btn-primary shadow-2 mb-4"/>
                    <p class="mb-2 text-muted">Forgot password? <a href="<?= base_url('reset/getresset');?>">Reset</a></p>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Required Js -->
    <script src="<?= base_url('assets/js/vendor-all.min.js');?>"></script><script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?= base_url('assets/s/pcoded.min.js')?>"></script>

</body>
</html>
