<?php
 if(!$this->session->userdata('admin')){
     redirect('Eror403');
     exit();
 }

 $sesi=$this->session->userdata('admin');


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title> Daftar User - R-Kinerja</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content="CodedThemes" />

    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url('assets/img/faviconR.ico');?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css');?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/animation/css/animate.min.css');?>">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/css/style2.css');?>">


</head>

<body>
    <div class="auth-wrapper aut-bg-img-side cotainer-fiuid align-items-stretch ">
        <div class="row align-items-center w-100 align-items-stretch bg-white " >
            <div class="d-none d-lg-flex col-lg-8 aut-bg-img align-items-center d-flex justify-content-center">
                <div class="col-md-8 fixed-top" style="margin-top:10%;">
                  <img class="logoharmoni" src="<?= base_url('assets/img/logoharmonipermata.png');?>"/>
                  <h1 align="center" class="text-white mb-5">Report Kinerja</h1>
                  <p align="center" class="text-white" >CV.Harmoni Permata</p>
                </div>
            </div>
            <div class="col-lg-4 align-items-stret h-100 align-items-center d-flex justify-content-center">
                <div class=" auth-content text-center">
                    <div class="mb-4">
                        <i class="feather icon-user-plus auth-icon"></i>
                    </div>
                    <h3 class="mb-4">Sign up</h3>
                    <?php
                        if($this->session->flashdata('sukses')){
                            echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('sukses')."</div>";

                        }elseif($this->session->flashdata('gagal')){
                            echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('gagal')."</div>";
                        }

                        ?>





                     <form id="form_profile"  method="post" action="<?= base_url('Register/postdata');?>">
                       <?php
                        $fcl=array('class'=>'form-control','id'=>'form');
                        echo form_open_multipart('Register/postdata',$fcl);
                       ?>
                    <div class="input-group mb-3">
                        <input type="text" name="NIK" class="form-control" placeholder="NIK"  required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="Nama" placeholder="Nama lengkap" required>
                    </div>
                    <div class="input-group mb-4">
                        <input type="text"  name="Username" class="form-control" placeholder="Username" required>
                    </div>

                    <div class="input-group mb-3">
                        <input type="Password" name="Password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="Password" name="Passwordconf" class="form-control" placeholder="Konfirmasi Password" required>
                    </div>
                    <div class="input-group mb-3">

                      <select class="form-control" name="Level">

                          <option value="administrator">administrator</option>
                          <option value="karyawan">karyawan</option>
                          <option value="manager">manager</option>
                          <option value="CEO">CEO</option>
                      </select>
                    </div>
                    <div class="input-group mb-3">
                      <select class="form-control" name="JenisKelamin">
                          <option value="L">Laki-Laki</option>
                          <option value="P">Perempuan</option>
                        </select>

                    </div>
                    <div class="input-group mb-3">
                        <input  type="text" name="NoTelpon" class="form-control" placeholder="No Telpon" required>
                    </div>
                    <div class="input-group mb-3">
                        <input  type="date" name="TglLahir" class="form-control" required/>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" name="Email" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="input-group mb-3">
                        <input  type="text" name="TargetJam" class="form-control" placeholder="Target jam kerja" required>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="Status" class="form-control" placeholder="Status" required>
                    </div>
                    <div class="input-group mb-3">
                      <select class="form-control" name="Jabatan">

                      <?php foreach ($hasil->result() as $result) : ?>
                          <option value="<?= $result->IdJabatan ?>"><?= $result->IdJabatan ?> <?= $result->Jabatan ?></option>

                          <?php endforeach; ?>
                      </select>
                    </div>


                  <p class="mb-0 text-muted"><a href="<?= base_url('Dasboard/admin');?>"> Kembali Ke Dashboard</a></p>

                    <button type="submit" id="klik" class="btn btn-primary shadow-2 mb-4" >Sign up</button>

                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>


    <!-- Required Js -->
    <script src="<?php echo base_url('assets/js/vendor-all.min.js');?>"></script><script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- <script src="<?php echo base_url('assets/js/pcoded.min.js');?>"></script> -->
    <script src="<?= base_url('assets/plugins/jquery/js/jquery.min.js');?>">
</script>




  </script>

</body>
</html>
