<?php
include('header.php');
?>


<!-- [ chat user list ] start -->
<section class="header-user-list">
    <div class="h-list-header">
        <div class="input-group">
            <input
                type="text"
                id="search-friends"
                class="form-control"
                placeholder="Search Friend . . .">
        </div>
    </div>
    <div class="h-list-body">
        <a href="dashboard-ecommerce.html#!" class="h-close-text">
            <i class="feather icon-chevrons-right"></i>
        </a>
        <div class="main-friend-cont scroll-div">
            <div class="main-friend-list">
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="1"
                    data-status="online"
                    data-username="Josephin Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . .
                            </small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="2"
                    data-status="online"
                    data-username="Lary Doe">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="3"
                    data-status="online"
                    data-username="Alice">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-3.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="4"
                    data-status="offline"
                    data-username="Alia">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-1.jpg');?>"
                        alt="Generic placeholder image">
                        <div class="live-status">1</div>
                    </a>
                    <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small>
                        </h6>
                    </div>
                </div>
                <div
                    class="media userlist-box"
                    data-id="5"
                    data-status="offline"
                    data-username="Suzen">
                    <a class="media-left" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius"
                        src="<?= base_url('assets/images/user/avatar-4.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- [ chat user list ] end -->

<!-- [ chat message ] start -->
<section class="header-chat">
    <div class="h-list-header">
        <h6>Josephin Doe</h6>
        <a href="dashboard-ecommerce.html#!" class="h-back-user-list">
            <i class="feather icon-chevron-left"></i>
        </a>
    </div>
    <div class="h-list-body">
        <div class="main-chat-cont scroll-div">
            <div class="main-friend-chat">
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius img-radius m-t-5"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">hello Datta! Will you tell me something</p>
                            <p class="chat-cont">about yourself?</p>
                        </div>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
                <div class="media chat-messages">
                    <div class="media-body chat-menu-reply">
                        <div class="">
                            <p class="chat-cont">Ohh! very nice</p>
                        </div>
                        <p class="chat-time">8:22 a.m.</p>
                    </div>
                </div>
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="dashboard-ecommerce.html#!"><img
                        class="media-object img-radius img-radius m-t-5"
                        src="<?= base_url('assets/images/user/avatar-2.jpg');?>"
                        alt="Generic placeholder image"></a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">can you help me?</p>
                        </div>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="h-list-footer">
        <div class="input-group">
            <input type="file" class="chat-attach" style="display:none">
            <a
                href="dashboard-ecommerce.html#!"
                class="input-group-prepend btn btn-success btn-attach">
                <i class="feather icon-paperclip"></i>
            </a>
            <input
                type="text"
                name="h-chat-text"
                class="form-control h-send-chat"
                placeholder="Write hear . . ">
            <button type="submit" class="input-group-append btn-send btn btn-primary">
                <i class="feather icon-message-circle"></i>
            </button>
        </div>
    </div>
</section>
<!-- [ chat message ] end -->

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->

                <!-- [ breadcrumb ] end -->
                <div class="main-body" id="divrefresh">
                <div class="row">
                   <div class="col">
                   <div class="col borderpage">
                     <div class="row">
                       <div class="col">
                         <?php
                         $datestring = '%d-%m-%Y ';
                         // $tgl=Date('d-m-y');
                         echo "<h5 style='font-size:2em;font-color:black;'>Tanggal :"." ".mdate($datestring)."</h5>";
                         ?>
                       </div>
                       <div class="col">
                         <h5 style="float:right;" class="hei-100   wid-100 bg-light d-inline-block mr-2 border border-secondary rounded" data-toggle="tooltip" >
                           <p style="font-size:2em;">
                             <?php
                             if ($sumkinerja>0) {
                               echo $sumkinerja;
                             }else{
                               echo 0;
                             }
                              ?>                            

                           </br>Jam</p>

                         </h5>
                       </div>
                       <div class="w-100"></div>
                       <div class="col">
                         <!-- start form -->
                         <?php
                          echo form_open('Jobdesk/tambahbyadmin');
                         ?>
                         <div class="row">
                           <div class="col-md-6">
                               <input type="text" class="form-control add_task_todo" name="Jobdesk" placeholder="Masukan Jobdesk anda" required>
                           </div>
                           <div class="col-sm-2">
                               <input type="text" class="form-control add_task_todo" name="Jam" placeholder="Jam" required>
                               <input type="hidden" name="NIK" value="<?= $this->session->userdata('NIK'); ?>">
                           </div>
                           <div class="col-sm">
                             <button  type="submit" name="addjob" class="btn btn-icon btn-info"><i class="feather icon-plus-circle"></i></button>

                           </div>
                         </div>
                       </form>
                         <!-- end form -->
                       </div>
                       <div class="w-100"></div>
                       <div class="col">
                         <div class="row">
                             <div class="col-sm-12">
                             <div class="card">
                             <div class="card-header">
                             <h5>Kinerja <?php echo $this->session->userdata('nama'); ?></h5>
                             <?php
                                 if($this->session->flashdata('sukses')){
                                     echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('sukses')."</div>";

                                 }elseif($this->session->flashdata('gagal')){
                                     echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('gagal')."</div>";
                                 }

                                 ?>
                             </div>
                             <div class="card-block">
                             <div class="table-responsive">

                                 <table id="zero-configuration" class="display table nowrap table-striped table-hover" style="width:100%">

                         <thead>
                             <tr>
                             <th>Description</th>
                             <th>Estimasi Per Jam</th>
                             <th>Action</th>

                             </tr>
                             </thead>
                             <tbody>
                               <?php foreach ($hasil->result() as $result) : ?>

                             <tr>
                               <input type="hidden" name="" value="<?= $result->NIK; ?>">
                             <td><?= $result->Jobdesk; ?></td>
                             <td id="jamkerja"><?= $result->EstimasiKerja;  ?></td>


                             <td>
                               <a link href="#" class="md-trigger" data-modal="datauser<?= $result->KodeJobdesk;?>"  data-target="#datauser<?= $result->KodeJobdesk;?>"><i class="feather icon-edit-2"></i></a>
                               | <a link href="<?= base_url('Jobdesk/deletebyadmin/'.$result->KodeJobdesk);?>"><i class="feather icon-trash-2" onclick="return confirm('Apakah Anda Ingin Menghapus Jobdesk ini');"></i></a>
                             </td>
                             </tr>
                               <?php endforeach; ?>
                             </tbody>
                             <tfoot>
                             <!-- <tr>
                             <th>Name</th>
                             <th>Position</th>
                             <th>Office</th>
                             <th>Age</th>
                             <th>Start date</th>
                             <th>Salary</th>
                             </tr> -->
                             </tfoot>
                             </table>
                             </div>
                             </div>
                             </div>
                             </div>
                             </div>
                       </div>

                       <!-- modal start -->


                       <?php
                    $no =1;
                      foreach ($hasil->result() as $result) :
                      ?>
                       <div class="md-modal md-effect-3" style="border:1px solid #b2bec3;"  id="datauser<?= $result->KodeJobdesk;?>">

                       <div class="md-content " >

                       <h3 class="theme-bg2">Edit data  <a link href="#" class="md-close" style="float:right;"><i class="feather icon-x"></i></a></h3>

                            <div id="datauser<?= $result->KodeJobdesk;?>">

                       <form name="edituser" method="POST" action="<?= base_url('Jobdesk/updatebyadmin/'.$result->KodeJobdesk)?>">
                         <div class="card-block table-border-style ">
                     <div class="table-responsive">
                     <table class="table ">

                     <!-- <thead>
                     <tr>
                     <th>#</th>
                     <th>First Name</th>
                     <th>Last Name</th>
                     <th>Username</th>
                     </tr>
                     </thead> -->
                     <tbody>

                     <tr>
                     <td>Jobdesk</td>
                     <td>:</td>
                     <td><input class="form-control" name="Jobdesk"  type="text" value="<?=$result->Jobdesk;?>"/></td>
                     <input type="hidden" name="TglJobdesk" value="<?= $result->Tanggal; ?>">
                     <input type="hidden" name="NIK" value="<?= $result->NIK; ?>">
                     </tr>
                     <tr>


                     <td>Estimasi</td>
                     <td>:</td>
                     <td><input class="form-control" name="Jam" type="text" value="<?=$result->EstimasiKerja;?>"/></td>
                     </tr>

                     </tbody>
                     </table>

                     </div>
                     </div>


                       <button type="submit" name="update" class="btn btn-primary" data-toggle="tooltip">Update</button>
                       </form>
                       <!-- <button class="btn btn-primary md-close">Close me!</button> -->

                       </div>

                       </div>

                       </div>
                         <?php endforeach; ?>
                       <!-- modal stop -->


                     </div>



                   </div>

                   </div>
                  <div class="col-sm-3">
                    <?php include('sidekanan.php'); ?>
                  </div>

                </div>


                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
</div>
</div>
</div>
</div>


<?php
include('footer.php');
?>
