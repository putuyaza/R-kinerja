<?php
 if(!$this->session->userdata('admin')){
     redirect('Eror403');
     exit();
 }


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Print Laporan Gaji - R-Kinerja</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Datta Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="admin templates, bootstrap admin templates, bootstrap 4, dashboard, dashboard templets, sass admin templets, html admin templates, responsive, bootstrap admin templates free download,premium bootstrap admin templates, datta able, datta able bootstrap admin template">
    <meta name="author" content="Codedthemes" />
    <!-- <meta http-equiv="refresh" content="10" /> -->

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('assets/images/favicon.ico');?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/animation/css/animate.min.css');?>">
    <!-- vendor css -->

    <link rel="stylesheet" href="<?= base_url('assets/css/style2.css');?>">
    <!-- bootstrap css-->

    <link rel="stylesheet" href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css');?>"/>

    <!-- Smart Wizard css -->
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_arrows.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_circles.min.css');?>" rel="stylesheet" />
    <link href="<?= base_url('assets/plugins/smart-wizard/css/smart_wizard_theme_dots.min.css');?>" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url('assets/plugins/data-tables/css/datatables.min.css');?>">
      <link rel="stylesheet" href="<?= base_url('assets/css/style.css');?>" >
      <!-- Notification css -->
<link href="<?= base_url('assets/plugins/notification/css/notification.min.css');?>" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url('assets/plugins/modal-window-effects/css/md-modal.css');?>">
<!-- Notification Js -->



    <script src="<?= base_url('assets/plugins/jquery/js/jquery.min.js');?>">
</script>
  </head>
  <body>

    <div class="row">
        <div class="col-sm-12">
        <div class="card">
        <div class="card-header">



          <table>
            <tr>
              <td width="550px"><img style="width:40%;" src="<?= base_url('assets/img/logoharmonipermata.png') ?>" /></td>
              <td>
                <h3 class="text-center" style="margin:0;display:block;">
                  Laporan Gaji</br>
                  CV.Harmoni Permata </br>
                  <?php
                  $datestring = '%Y ';
                  // $tgl=Date('d-m-y');
                  echo mdate($datestring);
                  ?></br>

                </h3>

              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
    <hr>
      <a href="<?= base_url('Dasboard/laporan_pdf') ?>" class="btn btn-primary"><i class="feather icon-printer" title="Print Laporan gaji"></i></a>
      <a href="<?= base_url('Dasboard/slipgaji') ?>" class="btn btn-success"><i class="feather icon-printer" title="Print slip gaji"></i></a>
</br>
    <div class="table-responsive">
          <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">

<thead>
    <tr>
    <th>NO</th>
    <th>Nama</th>
    <th>Invoice</th>
    <th>Target</th>
    <th>Total Jam</th>
    <th>Jabatan</th>
    <th>Gaji Pokok</th>
    <th>Bonus</th>
    <th>Tunjangan</th>
    <th>Total Gaji</th>
    <th>Status</th>
    </tr>
    </thead>

    <tbody>

      <?php
      $no =1;
      foreach ($hasilgaji->result() as $result){
        ?>

    <tr>
    <td><?= $no++; ?></td>
    <td><?= $result->Nama; ?></td>
    <td>GHP-<?= $result->InvoiceGaji; ?></td>
    <td><?= $result->TargetJam; ?></td>
    <td><?= $controller->sumdata($result->NIK);?></td>
    <td><?= $result->Jabatan; ?></td>
    <td>
      <?php
      if($controller->sumdata($result->NIK) >= $result->TargetJam ){
        echo "Rp &nbsp;".rupiah($gaji1 = $result->GajiPokok);
      }else{
         $gaji1 = ($result->GajiPokok/$result->TargetJam)*$controller->sumdata($result->NIK);
         echo "Rp &nbsp;".rupiah($gaji1);
      }
      ?>
    </td>
    <td><?= "Rp &nbsp;".rupiah($result->Bonus); ?> </td>
    <td>

      <?php if($result->Tunjangan > 0){
        echo "Rp &nbsp;".rupiah($result->Tunjangan);

      } else{
        echo "0";
      }

      ?>
    </td>
    <td>
      <?php
      $gajipokok = $gaji1;
      $tunjangan = $result->Tunjangan;
      $bonus = $result->Bonus;
      $gaji= $result->Gaji;
      if ($result->Gaji>0) {
        $totalsemua=$gaji + $tunjangan + $bonus;
          echo "Rp &nbsp;".rupiah($totalsemua);
      }else{
        $totalsemua=$gajipokok + $tunjangan + $bonus;
          echo "Rp &nbsp;".rupiah($totalsemua);

      }
      // echo "gajipoko &nbsp;".$gajipokok."</br>";
      //   echo "Tunjangan &nbsp;".$tunjangan."</br>";
      //     echo "Bonus &nbsp;".$bonus."</br>";


       ?>
   </td>
   <td>
     <?php
     $status=$result->Status;
    if ($status>0) {
      if($controller->sumdata($result->NIK) >= $result->TargetJam ){
        echo "<p>Target terpenuhi</p>";
      }else{
        echo "<p style='color:red'>Target tidak terpenuhi</p>";
      }
       echo "Sudah dibayar";
     }else{
       if($controller->sumdata($result->NIK) >= $result->TargetJam ){
         echo "Target terpenuhi";
       }else{
         echo "<p style='color:red'>Target tidak terpenuhi</p>";
       }
       echo "<p style='color:red'>Belum dibayar</p>";
     }
      ?>

   </td>
    </tr>

  <?php } ?>
    </tbody>

    <tfoot>
      <hr>
      <th>
      <td colspan="7">&nbsp;</td>
      <td>&nbsp;</td>
        <td style="text-center"> <p class="text-align:center;">Di sahkan oleh, </p> </td>
      </th>
    <tr>
      <td colspan="9">&nbsp;</td>
    <td class="text-center">
      MANAGER </br>
      &nbsp;</br>
      &nbsp;</br>
      &nbsp;</br>
      &nbsp;</br>
      Evan Jong

    </td>
    <td class="text-center">
      CEO </br>
      &nbsp;</br>
      &nbsp;</br>
      &nbsp;</br>
      &nbsp;</br>
      Hendika Permana
    </td>
    </tr>
    </tfoot>
    </table>



  </div>
</div>
</div>
</div>
</div>

    <!-- Required Js -->
    <script src="<?= base_url('assets/js/vendor-all.min.js');?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
      <script src="<?= base_url('assets/js/pcoded.min.js');?>"></script>
    <!-- <script src="<?= base_url('assets/js/menu-setting.min.js');?>"></script> -->

    <!-- amchart js -->
    <script src="<?= base_url('assets/plugins/amchart/js/amcharts.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/gauge.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/serial.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/light.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/pie.min.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/ammap.min.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/usaLow.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/radar.js');?>"></script>
    <script src="<?= base_url('assets/plugins/amchart/js/worldLow.js');?>"></script>


    <!-- dashboard-custom js -->
    <script src="<?= base_url('assets/js/pages/dashboard-ecommerce.js');?>"></script>

<script src="<?= base_url('assets/js/horizontal-menu.js');?>">
</script>
<script src="<?= base_url('assets/plugins/notification/js/bootstrap-growl.min.js');?>"></script>

<script src="<?= base_url('assets/plugins/modal-window-effects/js/classie.js');?>"></script>
<script src="<?= base_url('assets/plugins/modal-window-effects/js/modalEffects.js');?>"></script>
<script src="<?= base_url('assets/plugins/data-tables/js/datatables.min.js');?>"></script>

<!-- jquery js-->

<script type="text/javascript">

      //Collapse menu
      (function() {
          if ($('#layout-sidenav').hasClass('sidenav-horizontal') || window.layoutHelpers.isSmallScreen()) {
              return;
          }
          try {
              window.layoutHelpers.setCollapsed(
                  localStorage.getItem('layoutCollapsed') === 'true',
                  false
              );
          } catch (e) {}
      })();
      $(function() {
          // Initialize sidenav
          $('#layout-sidenav').each(function() {
              new SideNav(this, {
                  orientation: $(this).hasClass('sidenav-horizontal') ? 'horizontal' : 'vertical'
              });
          });

          // Initialize sidenav togglers
          $('body').on('click', '.layout-sidenav-toggle', function(e) {
              e.preventDefault();
              window.layoutHelpers.toggleCollapsed();
              if (!window.layoutHelpers.isSmallScreen()) {
                  try {
                      localStorage.setItem('layoutCollapsed', String(window.layoutHelpers.isCollapsed()));
                  } catch (e) {}
              }
          });
      });
      $(document).ready(function() {
          $("#pcoded").pcodedmenu({
              themelayout: 'horizontal',
              MenuTrigger: 'hover',
              SubMenuTrigger: 'hover',
          });
      });


</script>


<script src="<?= base_url('assets/plugins/data-tables/js/datatables.min.js');?>"></script>
<script src="<?= base_url('assets/js/pages/tbl-datatable-custom.js');?>"></script>
  </body>
</html>
