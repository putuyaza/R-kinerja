<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Model Register_model
 *
 * This Model for ...
 *
 * @package		CodeIgniter
 * @category	Model
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Register_model extends CI_Model {

  // ------------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  public function getAll()
	{
    // $this->db->select('*');
    // $this->db->from('user');
    // $this->db->join('jabatan', 'user.IdJabatan = jabatan.IdJabatan');
    // return $this->db->get();
    return $this->db->get('jabatan');

  }
  public function simpandata($dataregister)
  {
    $data = $this->db->insert('user', $dataregister);

   return $data;


  }

  public function updateuser($dataregister,$NIK){
    $this->db->where('NIK',$NIK);
    $data = $this->db->update('user',$dataregister);
    return $data;
  }

  public function getperuser($NIK){
    $this->db->where('NIK',$NIK);
    $this->db->select('*');
    $this->db->from('user');
    $this->db->join('jabatan','jabatan.IdJabatan=user.IdJabatan');
  return  $hasil = $this->db->get();
  }

  public function updateperuser($dataregister,$NIK){
      $this->db->where('NIK',$NIK);
      $data = $this->db->update('user',$dataregister);
      return $data;
  }


  // ------------------------------------------------------------------------

}

/* End of file Register_model.php */
/* Location: ./application/models/Register_model.php */
