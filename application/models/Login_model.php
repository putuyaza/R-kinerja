
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Model Login_model_model
 *
 * This Model for ...
 * 
 * @package		CodeIgniter
 * @category	Model
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Login_model extends CI_Model {

  // ------------------------------------------------------------------------

  public function __construct()
  {
    parent::__construct();
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  public function proseslogin($username,$password){
    
    $this->db->join('jabatan', 'user.IdJabatan = jabatan.IdJabatan');
    $hasil=$this->db->get_where('user',array('Username' =>$username,'Password'=>$password))->row();
    $level=$hasil->Level;
    $nama=$hasil->Nama;

    if($hasil->Username==$username && $hasil->Password==$password){
    //  $sesinama= $this->session->set_userdata('Nama',$nama);
    //  $sesilevel= $this->session->set_userdata('Level',$level);
        return $hasil;
  
      }else{
        $this->session->set_flashdata('msg', 'Maaf username dan password yang anda masukan salah');
        
      redirect('login');
      }

      
  
    }

  // ------------------------------------------------------------------------

}


/* End of file Login_model_model.php */
/* Location: ./application/models/Login_model_model.php */