<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobdesk_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  public function tambah($datajobdesk){

    $data = $this->db->insert('jobdesk',$datajobdesk);
    return $data;
  }

  public function getperjob($NIK){
    $tglhariini = date('Y-m-d');
    $this->db->where('NIK',$NIK);
    $this->db->where('Tanggal',$tglhariini);
    $this->db->select('*');
    $this->db->from('jobdesk');
    return $hasil = $this->db->get();

  }
  public function getjobperkaryawan($NIK){
    $this->db->order_by('Tanggal','DESC');
    $this->db->where('NIK',$NIK);
    $this->db->select('*');
    $this->db->from('jobdesk');
    return $hasil = $this->db->get();
  }


  public function getsum($NIK){
    $tglhariini = date('Y-m-d');
    $sql=" SELECT sum(EstimasiKerja) AS EstimasiKerja FROM jobdesk WHERE Tanggal='$tglhariini' AND NIK='$NIK'";
    $hasil = $this->db->query($sql);
    return $hasil->row()->EstimasiKerja;
  }

  public function getsumkinerja($NIK){
    $sql=" SELECT sum(EstimasiKerja) AS EstimasiKerja FROM jobdesk WHERE NIK='$NIK'";
    $hasil = $this->db->query($sql);
    return $hasil->row()->EstimasiKerja;
  }



  public function update($datajobdesk,$KodeJobdesk){
    $this->db->where('KodeJobdesk',$KodeJobdesk);
    return $data = $this->db->update('jobdesk',$datajobdesk);
  }
  public function getalljob(){
    $this->db->select('*');
    $this->db->from('jobdesk');
    $this->db->join('user','user.NIK=jobdesk.NIK');
    $this->db->group_by('jobdesk.NIK');
    return $data = $this->db->get();

  }

public function getgaji(){
$this->db->select('*');
$this->db->from('jobdesk');
$this->db->join('user','user.NIK=jobdesk.NIK');
$this->db->join('jabatan','jabatan.IdJabatan=user.IdJabatan');
$this->db->join('kinerja','kinerja.NIK=jobdesk.NIK');
$this->db->group_by('jobdesk.NIK');
return $data = $this->db->get();
}


public function tambahkinerja($NIK,$gajikinerja){
  $this->db->where('NIK',$NIK);
  return $data=$this->db->update('kinerja',$gajikinerja);

}

public function posting($datakinerja){
return $data = $this->db->insert('kinerja',$datakinerja);
}

}
